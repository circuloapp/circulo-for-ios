//
//  CirculoNotificationsViewController.swift
//  Circulo
//
//  Created by N-Pex on 30.11.21.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

class CirculoNotificationsViewController: FormViewController {
    
    // MARK: Properties
    var session: MXSession? {
        didSet {
            if self.isViewLoaded {
                self.createForm()
            }
        }
    }
    
    var notificationEnabled: Bool {
        get {
            if let nc = self.session?.notificationCenter, let rule = nc.rule(byId: ".m.rule.master") {
                return !rule.enabled
            }
            return true
        }
    }
    
    public convenience init() {
        self.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: Public Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Notifications".localize()

        tableView.tintColor = .accent

        createForm()

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.heightConstraint = 304

        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .systemGroupedBackground

        view.addSubview(container)

        container.topAnchor.constraint(equalTo: tableView.bottomAnchor).isActive = true
        container.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        container.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        container.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(openSettings), for: .touchUpInside)
        button.setTitle("Open Settings".localize())
        button.setTitleColor(.white)
        button.backgroundColor = .accent
        button.cornerRadius = 24

        container.addSubview(button)

        button.topAnchor.constraint(equalTo: container.topAnchor, constant: 8).isActive = true
        button.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 16).isActive = true
        button.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -16).isActive = true
        button.heightConstraint = 48
    }

    open func createForm() {
        form.removeAll()

        form
        +++ Section(footer: "If someone shares their location or sends a message, you will receive a basic notification".localize())

        <<< TextRow("notifications") {
            $0.title = "Notifications".localize()
            $0.disabled = true
        }
        .cellUpdate { cell, _ in
            cell.titleLabel?.textColor = .label
        }

        +++ Section(footer: "If someone sends an alert, you will receive a critical alert. Critical alerts appear on the lock screen and play a sound, even if a focus is on or the iPhone is muted.".localize())

        <<< TextRow("critical") {
            $0.title = "Critical Alerts".localize()
            $0.disabled = true
        }
        .cellUpdate { cell, _ in
            cell.titleLabel?.textColor = .label
        }

        Task {
            let settings = await UNUserNotificationCenter.current().notificationSettings()

            await MainActor.run {
                let notificationsRow = form.rowBy(tag: "notifications") as? TextRow

                if settings.alertSetting == .enabled && settings.alertStyle != .none {
                    notificationsRow?.value = "On".localize()
                }
                else {
                    notificationsRow?.value = "Off".localize()
                }

                notificationsRow?.updateCell()

                let criticalRow = form.rowBy(tag: "critical") as? TextRow

                if settings.criticalAlertSetting == .enabled {
                    criticalRow?.value = "Enabled".localize()
                }
                else {
                    criticalRow?.value = "Disabled".localize()
                }

                criticalRow?.updateCell()
            }
        }
    }

    @IBAction
    func openSettings() {
        if let url = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(url)
        }
    }
}
