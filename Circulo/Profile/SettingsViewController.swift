//
//  SettingsViewController.swift
//  Circulo
//
//  Created by N-Pex on 12.10.21.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

class SettingsViewController: FormViewController {

    public let languageRow = ButtonRow() {
        $0.title = "Language".localize()
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.textAlignment = .natural
            cell.tintColor = .accent
    }

    public convenience init() {
        self.init(style: .grouped)
    }
    
    public override init(style: UITableView.Style) {
        super.init(style: style)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Preferences".localize()

        form
        +++ languageRow
            .onCellSelection({ cell, row in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url)
                }
            })

        +++ Section("Alert Preferences".localize())

        <<< SwitchRow {
            $0.title = "Allow access to microphone".localize()
            $0.value = Settings.allowAccessToMicrophone && Self.isRecordAllowed()
            $0.cellStyle = .subtitle
        }
        .cellUpdate({ cell, _ in
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.text = "Used to send an audio memo with your alert.".localize()
            cell.detailTextLabel?.numberOfLines = 0
        })
        .onChange({ row in
            if Settings.allowAccessToMicrophone {
                Settings.allowAccessToMicrophone = false

                row.value = false
                row.updateCell()
            }
            else {
                Settings.allowAccessToMicrophone = true

                if #available(iOS 17.0, *) {
                    AVAudioApplication.requestRecordPermission(completionHandler: { granted in
                        row.value = granted
                        row.updateCell()
                    })
                }
                else {
                    AVAudioSession.sharedInstance().requestRecordPermission({ granted in
                        row.value = granted
                        row.updateCell()
                    })
                }

            }
        })

        <<< SwitchRow {
            $0.title = "Send location with alerts".localize()
            $0.value = Settings.sendLocationWithAlerts && Self.isLocationAllowed(LocationManager.shared.status)
            $0.cellStyle = .subtitle

            // Parental controls deny changing this. No point in trying.
            if LocationManager.shared.status == .restricted {
                $0.disabled = true
            }
        }
        .cellUpdate { cell, _ in
            cell.textLabel?.numberOfLines = 0
            cell.detailTextLabel?.text = "Used to share your location while the alert is active.".localize()
            cell.detailTextLabel?.numberOfLines = 0
        }
        .onChange { row in
            if Settings.sendLocationWithAlerts {
                Settings.sendLocationWithAlerts = false

                row.value = false
                row.updateCell()
            }
            else {
                Settings.sendLocationWithAlerts = true

                LocationManager.shared.requestAuthorization { status in
                    row.value = Self.isLocationAllowed(status)
                    row.updateCell()
                }
            }
        }

        +++ LabelRow() {
                $0.title = "Version: %, Build: %".localize(values: Bundle.main.version, Bundle.main.build)
            }
            .cellUpdate { cell, _ in
                cell.textLabel?.font = .italicSystemFont(ofSize: UIFont.smallSystemFontSize)
            }
    }


    // MARK: Private Methods

    private class func isRecordAllowed() -> Bool {
        if #available(iOS 17.0, *) {
            switch AVAudioApplication.shared.recordPermission {
            case .granted:
                return true

            default:
                return false
            }
        }
        else {
            switch AVAudioSession.sharedInstance().recordPermission {
            case .granted:
                return true

            default:
                return false
            }
        }
    }

    private class func isLocationAllowed(_ status: CLAuthorizationStatus) -> Bool {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse, .authorized:
            return true

        default:
            return false
        }
    }
}
