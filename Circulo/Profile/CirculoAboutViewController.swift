//
//  CirculoAboutViewController.swift
//  Circulo
//
//  Created by N-Pex on 12.08.21.
//

import UIKit
import MatrixSDK
import KeanuCore
import Eureka
import ViewRow

class CirculoAboutViewController: FormViewController {

    public convenience init() {
        self.init(style: .grouped)
    }

    public override init(style: UITableView.Style) {
        super.init(style: style)
    }

    required public init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    // MARK: Public Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "About".localize()

//        form +++ LabelRow() {
//            $0.title = "View Resources".localize()
//        }
//        .cellSetup({ cell, row in
//            cell.imageView?.image = .notifyNormal
//            if let section = row.section {
//                section.header = AboutHeaderView()
//            }
//        })
        form +++ LabelRow() {
            $0.title = "Version: %, Build: %".localize(values: Bundle.main.version, Bundle.main.build)
        }
        .cellSetup({ cell, row in
            if let section = row.section {
                section.header = AboutHeaderView()
            }
        })
        .cellUpdate { cell, _ in
            cell.textLabel?.font = .italicSystemFont(ofSize: UIFont.smallSystemFontSize)
        }
    }
}

fileprivate class AboutHeaderView : HeaderFooterViewRepresentable {
    func viewForSection(_ section: Section, type: HeaderFooterType) -> UIView? {
        let header = UITextView(forAutoLayout:())
        header.textContainerInset = UIEdgeInsets(top: 8,left: 8,bottom: 8,right: 8)
        header.isEditable = false
        header.font = UIFont(name: "Inter-Regular", size: 14)
        header.text = "Círculo was created by the Guardian Project and ARTICLE 19, based on the experience, needs and concerns of women who occupy a central role in society by working in the journalistic activity, social initiatives and mobilizations for the defense of human rights.\n\nAt the heart of the app is wellness, ensuring that each journalist, activist and human rights defender is supported by a self-chosen community. This can help to mitigate burnout and trauma and consequently self-censorship in what are high-risk fields of work.".localize()
        return header
    }

    var title: String?

    var height: (() -> CGFloat)?
}
