//
//  GroupCodeViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 28.04.23.
//

import UIKit

class GroupCodeViewController: UIViewController {

    @IBOutlet weak var groupCodeTf: UITextField! {
        didSet {
            groupCodeTf.text = CirculoSettings.shared.cleanInsightsGroupCode
        }
    }
    @IBOutlet weak var enterBt: UIButton!
    @IBOutlet weak var contributeWithoutBt: UIButton!


    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardOnOutsideTap()
    }


    @IBAction
    func grant(_ button: UIButton) {
        let code = (groupCodeTf.text ?? "").filter("0123456789".contains)

        if button == enterBt {
            if code.isEmpty {
                return errorHighlight()
            }

            CirculoSettings.shared.cleanInsightsGroupCode = code
        }
        else {
            if !code.isEmpty {
                return errorHighlight()
            }

            CirculoSettings.shared.cleanInsightsGroupCode = nil
        }

        let oldState = CleanInsightsManager.shared.state

        CleanInsightsManager.shared.grant()

        CleanInsightsManager.shared.measure(event: oldState == .denied ? "re-consented" : "consented")

        if let vc = navigationController?.viewControllers.last(where: { $0 is FeedbackViewController }) {
            navigationController?.popToViewController(vc, animated: true)
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }

    private func errorHighlight() {
        let old = groupCodeTf.backgroundColor

        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.groupCodeTf.backgroundColor = .systemOrange
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            UIView.animate(withDuration: 0.5) {
                self?.groupCodeTf.backgroundColor = old
            }
        }
    }
}
