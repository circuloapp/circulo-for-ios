//
//  CirculoAccountViewController.swift
//  Circulo
//
//  Created by N-Pex on 06-09-21.
//

import Keanu
import KeanuCore
import Eureka

public class CirculoAccountViewController: AccountViewController {
        
    override open func viewWillAppear(_ animated: Bool) {
        navigationItem.title = "Account".localize()
        super.viewWillAppear(animated)
                
        // Reset nav bar after core has messed it up.
        if let bar = navigationController?.navigationBar {
            bar.setBackgroundImage(nil, for: .default)
            bar.tintColor = .label
            bar.shadowImage = nil
            bar.barStyle = .default
        }
    }
}
