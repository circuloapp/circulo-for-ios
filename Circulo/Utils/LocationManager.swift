//
//  LocationManager.swift
//  Circulo
//
//  Created by N-Pex on 4.8.21.
//

import Foundation
import CoreMotion
import CoreLocation
import Contacts
import Keanu

extension CLLocation {
    open override var description: String {
        "geo:\(coordinate.latitude),\(coordinate.longitude)"
    }

    convenience init?(uri: String?) {
        guard let parts = uri?.components(separatedBy: ":"),
              parts.count > 1
        else {
            return nil
        }

        let coord = parts[1].components(separatedBy: ",")

        guard coord.count > 1,
              let lat = Double(coord[0]),
              let lon = Double(coord[1])
        else {
            return nil
        }

        self.init(latitude: lat, longitude: lon)
    }
}

extension Notification.Name {

    static let locationTrackingEnded = Notification.Name("location-tracking-ended")
    static let locationError = Notification.Name("location-error")
}

class LocationManager: NSObject, CLLocationManagerDelegate {

    public enum Duration: CaseIterable, CustomStringConvertible, Equatable {

        public static var allCases = [Self.onetime, .minutes15, .minutes60, .minutes240, .minutes480]

        case year1
        case minutes480
        case minutes240
        case minutes60
        case minutes15
        case onetime
        case until(arrival: Date)

        init(rawValue: Int) {
            switch rawValue {
            case 0:
                self = .onetime

            case 1:
                self = .minutes15

            case 2:
                self = .minutes60

            case 3:
                self = .minutes240

            case 4:
                self = .minutes480

            case .max:
                self = .year1

            default:
                self = .until(arrival: .init(timeIntervalSince1970: TimeInterval(rawValue)))
            }
        }

        var description: String {
            switch self {
            case .year1:
                return "1 Year".localize()

            case .minutes480:
                return "8 hours".localize()

            case .minutes240:
                return "4 hours".localize()

            case .minutes60:
                return "1 hour".localize()

            case .minutes15:
                return "15 minutes".localize()

            case .onetime:
                return "right now".localize()

            case .until(_):
                return "Until I arrive".localize()
            }
        }

        var length: TimeInterval {
            switch self {
            case .year1:
                return 365 * 24 * 60 * 60

            case .minutes480:
                return 8 * 60 * 60

            case .minutes240:
                return 4 * 60 * 60

            case .minutes60:
                return 60 * 60

            case .minutes15:
                return 15 * 60

            case .onetime:
                return 0

            case .until(let arrival):
                return arrival.timeIntervalSinceNow
            }
        }

        /**
         UI update interval, depending on overall length in seconds.
         */
        var interval: TimeInterval {
            if case .until = self {
                if length < Duration.minutes15.length {
                    return Duration.minutes15.interval
                }

                if length < Duration.minutes60.length {
                    return Duration.minutes60.interval
                }

                if length < Duration.minutes240.length {
                    return Duration.minutes240.interval
                }

                return Duration.minutes480.interval
            }


            switch self {
            case .minutes480, .until:
                return 60

            case .minutes240:
                return 60

            case .minutes60:
                return 60

            case .minutes15, .year1:
                return 15

            case .onetime:
                return 0
            }
        }

        var rawValue: Int {
            switch self {
            case .onetime:
                return 0

            case .minutes15:
                return 1

            case .minutes60:
                return 2

            case .minutes240:
                return 3

            case .minutes480:
                return 4

            case .year1:
                return .max

            case .until(let arrival):
                return Int(arrival.timeIntervalSince1970)
            }
        }
    }

    typealias AuthorizationCallback = (CLAuthorizationStatus) -> Void


    static let shared = LocationManager()

    private static let addressFormatter = CNPostalAddressFormatter()
    private static let locationFormatter: TTTLocationFormatter = {
        let formatter = TTTLocationFormatter()
        formatter.coordinateStyle = .degreesMinutesSecondsFormat

        return formatter
    }()

    private static let distanceFilter = 10.0 // in meters.

    var status: CLAuthorizationStatus {
        manager.authorizationStatus
    }

    var isTracking: Bool {
        DispatchWallTime.now() < trackingTimeout
    }


    private lazy var manager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self

        return manager
    }()

    private var trackingTimeout: DispatchWallTime = .now()
    private var trackingEnd: Date = .now

    private var trackingInterval: TimeInterval = 0
    private var trackingNextTimeout: DispatchWallTime = .now()
    private var trackingKillTimer: Timer?

    private var authorizationCallback: AuthorizationCallback?


    private override init() {
    }


    // MARK: Public Methods

    func requestAuthorization(_ callback: @escaping AuthorizationCallback) {
        guard status == .notDetermined else {
            return callback(status)
        }

        authorizationCallback = callback

        manager.requestAlwaysAuthorization()
    }

    func trackLocation(_ duration: Duration) {
        trackingTimeout = DispatchWallTime.now() + duration.length
        trackingEnd = .now.addingTimeInterval(duration.length)
        trackingInterval = duration.interval
        trackingNextTimeout = .now()

        DispatchQueue.main.async {
            self.trackingKillTimer?.invalidate()

            // If the user doesn't move, we need to make sure and "end" gets triggered anyway!
            self.trackingKillTimer = Timer.scheduledTimer(
                timeInterval: duration.length, target: self,
                selector: #selector(self.cancel(timer:)), userInfo: nil, repeats: false)
        }

        manager.distanceFilter = Self.distanceFilter
        manager.startUpdatingLocation()
    }

    func cancel(notify: Bool = true) {
        manager.stopUpdatingLocation()
        trackingTimeout = .now()

        DispatchQueue.main.async {
            self.trackingKillTimer?.invalidate()
            self.trackingKillTimer = nil

            NotificationCenter.default.post(name: .locationTrackingEnded, object: nil)

            if notify {
                (PushManager.shared as! CirculoPushManager).notifyLocationSharingEnd()
            }
        }
    }

    @objc
    private func cancel(timer: Timer) {
        cancel(notify: true)
    }


    // MARK: CLLocationManagerDelegate
    
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if DispatchWallTime.now() > trackingTimeout {
            cancel()
        }

        if DispatchWallTime.now() > trackingNextTimeout {
            trackingNextTimeout = DispatchWallTime.now() + trackingInterval

            if !locations.isEmpty {
                Task {
                    for location in locations {
                        let name = await LocationManager.geocode(location)

                        try await Circles.shared.me()?.addLocation(location, with: name, until: trackingEnd)
                    }
                }
            }
            else {
                NotificationCenter.default.post(name: .locationError, object: CLError(.locationUnknown))
            }
        }
    }
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if (error as? CLError)?.code == .denied {
            return cancel()
        }

        NotificationCenter.default.post(name: .locationError, object: error)
    }

    internal func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        authorizationCallback?(manager.authorizationStatus)

        authorizationCallback = nil
    }


    // MARK: Static Methods

    class func format(_ location: CLLocation) -> String {
        return locationFormatter.string(from: location)
    }

    class func geocode(_ location: CLLocation?) async -> String {
        guard let location = location else {
            return "Unknown".localize()
        }

        return await withCheckedContinuation { continuation in
            CLGeocoder().reverseGeocodeLocation(location) { placemark, error in
                let name: String

                if let address = placemark?.first?.postalAddress {
                    name = addressFormatter.string(from: address).replacingOccurrences(of: "\n", with: ", ")
                }
                else {
                    name = format(location)
                }

                continuation.resume(returning: name)
            }
        }
    }
}
