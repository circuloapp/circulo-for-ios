//
//  CirculoPushManager.swift
//  Circulo
//
//  Created by N-Pex on 07.10.21.
//

import Keanu
import KeanuCore

class CirculoPushManager: PushManager {

    private static let circuloThreadPrefix = "circulo:"

    override class var authorizationOptions: UNAuthorizationOptions {
        [.badge, .alert, .sound, .providesAppNotificationSettings, .criticalAlert]
    }

    override func updateNotificationAndBadge(_ alertUser: Bool) {
        if Circles.shared.sessionReady {
            Task { @MainActor in
                let newMessages = Circles.shared.rooms
                    .map({ Circles.shared.members(of: $0) })
                    .joined()
                    .reduce(0) {
                        $0 + ($1.haveNotRecognizedAll ? 1 : 0)
                    }
                
                updateBadge(newMessages)
                updateNotification(alertUser, UInt(newMessages), 1)
            }
        }
    }
    
    override func updateNotification(_ alertUser: Bool, _ missedNotifications: UInt, _ missedDiscussions: UInt) {
        // Ignore
    }
    
    override func userNotificationCenter(
        _ center: UNUserNotificationCenter, willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            if notification.request.content.threadIdentifier.starts(with: CirculoPushManager.circuloThreadPrefix) {
                completionHandler([.sound, .banner])
                return
            }
            let userInfo = notification.request.content.userInfo
            if let roomId = userInfo["room_id"] as? String, let eventId = userInfo["event_id"] as? String {
                if let session = Circles.shared.session {
                    session.event(withEventId: eventId, inRoom: roomId) { result in
                        if result.isSuccess, let event = result.value, event.showNotification {
                            self.addLocalNotificationForEvent(event, inSession: session)
                        }
                        completionHandler([])
                    }
                    return
                }
            }
            // No other local notifications!
            // super.userNotificationCenter(center, willPresent: notification, withCompletionHandler: completionHandler)
            completionHandler([])
        }
    
    open func addLocalNotificationForEvent(_ event: MXEvent, inSession session: MXSession) {
        let nc = UNUserNotificationCenter.current()
        nc.getNotificationSettings { settings in
            // Do not schedule notifications if not authorized.
            guard settings.authorizationStatus == .authorized else {
                nc.removeAllDeliveredNotifications()
                return
            }
            
            // Build the notification.
            let localNotification = UNMutableNotificationContent()
            localNotification.body = event.notificationBody
            localNotification.sound = event.getNotificationSound(settings.criticalAlertSetting == .enabled)
            localNotification.threadIdentifier = CirculoPushManager.circuloThreadPrefix + event.eventId
            DispatchQueue.main.async {
                let request = UNNotificationRequest(
                    identifier: CirculoPushManager.circuloThreadPrefix + event.eventId,
                    content: localNotification, trigger: nil) // Schedule the notification.
                
                nc.add(request) { error in
                    if let error = error as NSError? {
                        Log.error(self, "Error scheduling notification:", error)
                    }
                }
            }
        }
    }

    override func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        if
            let rootVc = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.rootViewController as? RootViewController,
            let homeVc = (rootVc.viewControllers?.first as? UINavigationController)?.viewControllers.first as? HomeViewController,
            let profileVc = homeVc.viewControllers?.first(where: { $0 is CirculoProfileViewController }) as? CirculoProfileViewController,
            let idx = homeVc.viewControllers?.firstIndex(of: profileVc)
        {
            homeVc.selectedIndex = idx

            profileVc.showNotificationSettings()
        }
    }

    override func userNotificationCenter(_ center: UNUserNotificationCenter, 
                                         didReceive response: UNNotificationResponse,
                                         withCompletionHandler completionHandler: @escaping () -> Void)
    {
        switch response.actionIdentifier {
        case "locSharingEnd.viewAction":
            MyLocationView.shared.openCurrentLocationVc()

            completionHandler()

        case "locSharingEnd.deleteAction":
            if let vc = MyLocationView.shared.openCurrentLocationVc() {
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    vc.stop(force: true)
                }
            }

            completionHandler()

        default:
            super.userNotificationCenter(center, didReceive: response, withCompletionHandler: completionHandler)
        }
    }

    override func getAdditionalCategories() -> Set<UNNotificationCategory> {
        let viewAction = UNNotificationAction(
            identifier: "locSharingEnd.viewAction",
            title: "View".localize(),
            options: .foreground)

        let deleteAction = UNNotificationAction(
            identifier: "locSharingEnd.deleteAction",
            title: "Delete".localize(),
            options: [.foreground, .destructive])

        let category = UNNotificationCategory(
            identifier: "locSharingEnd",
            actions: [viewAction, deleteAction],
            intentIdentifiers: [],
            options: .customDismissAction)

        return [category]
    }

    func notifyLocationSharingEnd() {
        let content = UNMutableNotificationContent()
        content.title = "Location share complete!".localize()
        content.body = " Your history will be available to your circle until you delete it.".localize()
        content.categoryIdentifier = "locSharingEnd"
        content.sound = .init(named: .init(rawValue: "alert1.m4r"))

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)

        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)

        UNUserNotificationCenter.current().add(request) { error in
            Log.error(self, nil, error)
        }
    }
}
