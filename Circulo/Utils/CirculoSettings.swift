//
//  CirculoSettings.swift
//  Circulo
//
//  Created by N-Pex on 01.12.21.
//

import Foundation

class CirculoSettings {

    public static let shared = CirculoSettings()


    private let defaults = UserDefaults(suiteName: Config.appGroupId)


    private init() {
    }


    public var cleanInsightsGroupCode: String? {
        get {
            defaults?.string(forKey: "ci_group_code")
        }
        set {
            defaults?.set(newValue, forKey: "ci_group_code")
        }
    }
}
