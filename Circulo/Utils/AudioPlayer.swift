//
//  AudioPlayer.swift
//  Circulo
//
//  Created by Benjamin Erhart on 17.05.24.
//

import AVFoundation

protocol AudioPlayerDelegate: AnyObject {

    func audioPlayerDidFinish()

    func audioPlayerError(_ error: Error)

    func audioPlayerTimeUpdate(_ duration: TimeInterval, _ currentTime: TimeInterval)
}

class AudioPlayer: NSObject, AVAudioPlayerDelegate {

    // MARK: Public Properties

    var isPlaying: Bool {
        avPlayer?.isPlaying ?? false
    }

    var currentTime: TimeInterval {
        avPlayer?.currentTime ?? 0
    }

    var duration: TimeInterval {
        setup()

        return avPlayer?.duration ?? 0
    }

    weak var delegate: AudioPlayerDelegate?

    var error: Error? {
        alert.audioError ?? avError
    }


    // MARK: Private Properties

    private let alert: Alert
    private var avPlayer: AVAudioPlayer?
    private var avError: Error?
    private var timer: Timer?


    init(_ alert: Alert) {
        self.alert = alert
    }


    // MARK: Public Methods


    func play(fallback: URL? = nil) {
        setup(fallback)

        if avPlayer != nil {
            avPlayer?.play()

            if delegate != nil {
                timer = .scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateDuration), userInfo: nil, repeats: true)
            }
        }
        else {
            delegate?.audioPlayerDidFinish()
        }
    }

    func stop() {
        timer?.invalidate()
        timer = nil

        avPlayer?.stop()
    }


    // MARK: AVAudioPlayerDelegate

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        stop()

        delegate?.audioPlayerDidFinish()
    }

    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: (any Error)?) {
        if let error = error {
            avError = error

            delegate?.audioPlayerError(error)
        }

        stop()

        delegate?.audioPlayerDidFinish()
    }


    // MARK: Private Methods

    private func setup(_ fallback: URL? = nil) {
        guard avPlayer == nil else {
            return
        }

        let audio: Data?

        if alert.audio == nil, let fallback = fallback {
            audio = try? Data(contentsOf: fallback)
        }
        else {
            audio = alert.audio
        }

        guard let audio = audio else {
            return
        }

        do {
            try AVAudioSession.sharedInstance().setCategory(
                .playAndRecord, mode: .default,
                options: [.allowAirPlay, .allowBluetooth, .allowBluetoothA2DP,
                          .defaultToSpeaker, .duckOthers])

            avPlayer = try AVAudioPlayer(data: audio)
            avPlayer?.delegate = self
        }
        catch {
            avError = error

            delegate?.audioPlayerError(error)
        }
    }

    @objc
    private func updateDuration() {
        delegate?.audioPlayerTimeUpdate(duration, currentTime)
    }
}
