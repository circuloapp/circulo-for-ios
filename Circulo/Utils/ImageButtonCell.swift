//
//  ImageButtonCell.swift
//  Circulo
//
//  Created by N-Pex on 08.10.21.
//

import UIKit

open class ImageButtonCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!

    @objc public dynamic var iconColor: UIColor = .label {
        didSet {
            icon.tintColor = iconColor
        }
    }

    @objc public dynamic var labelColor: UIColor = .label {
        didSet {
            label.textColor = labelColor
        }
    }

    /**
     Applies an image icon and an action label to this cell.

     - parameter icon: The icon image to use.
     - parameter action: The text label content.
     - returns: self for convenience.
     */
    @discardableResult
    public func apply(_ icon: UIImage?, _ action: String?) -> ImageButtonCell {
        self.icon.image = icon?.withRenderingMode(.alwaysTemplate)
        self.label.text = action
        return self
    }
}
