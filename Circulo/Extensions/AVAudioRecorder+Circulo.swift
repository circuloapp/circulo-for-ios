//
//  AVAudioRecorder+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 29.02.24.
//

import AVFoundation

extension AVAudioRecorder {

    //Values for human speech range quiet to loud.
    private static let minDb = Float(-80)
    private static let maxDb = Float(-10)

    private static let powerFactor = Float(20)

    private static let minDbScale = pow(10, minDb / powerFactor)
    private static let maxDbScale = pow(10, maxDb / powerFactor)
    private static let dbDelta = maxDbScale - minDbScale

    private static let scaleMin = Float(0)
    private static let scaleMax = Float(1)
    private static let delta = scaleMax - scaleMin


    func scaledPower(forChannel channelNumber: Int) -> CGFloat {
        updateMeters()
        let db = averagePower(forChannel: channelNumber)

        var scale: CGFloat

        if db >= Self.maxDb {
            // Too loud.
            scale = 1
        }
        else if db <= Self.minDb {
            // Too soft.
            scale = 0
        }
        else {
            // Normal range.
            let linearScale = pow(10, db / Self.powerFactor)

            //Get a value between 0 and 1 for mindB & maxdB values.
            scale = CGFloat(((Self.delta * (linearScale - Self.minDbScale)) / Self.dbDelta) + Self.scaleMin)
        }

        return scale
    }
}
