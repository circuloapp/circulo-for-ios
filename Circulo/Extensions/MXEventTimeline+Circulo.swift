//
//  MXEventTimeline+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 24.09.24.
//

import Foundation
import MatrixSDK

extension MXEventTimeline {

    @discardableResult
    func paginate(_ numItems: UInt, direction: MXTimelineDirection, onlyFromStore: Bool) async -> MXResponse<Void> {
        await withCheckedContinuation { continuation in
            paginate(numItems, direction: direction, onlyFromStore: onlyFromStore) { response in
                continuation.resume(returning: response)
            }
        }
    }
}
