//
//  MXEventContentLocation+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 09.02.24.
//

import MatrixSDK
import CoreLocation
import MapKit
import Contacts

extension MXEventContentLocation {

    var location2d: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }

    var location: CLLocation {
        CLLocation(latitude: latitude, longitude: longitude)
    }

    var placemark: MKPlacemark {
        if let street = locationDescription, !street.isEmpty {
            let address = CNMutablePostalAddress()
            address.street = street

            return MKPlacemark(coordinate: location2d, postalAddress: address)
        }

        return MKPlacemark(coordinate: location2d)
    }
}
