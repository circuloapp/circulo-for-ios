//
//  UIFont+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 27.04.23.
//

import UIKit

extension UIFont {

    var bold: UIFont? {
        guard let descriptor = fontDescriptor.withSymbolicTraits(.traitBold) else {
            return nil
        }

        return UIFont(descriptor: descriptor, size: 0)
    }
}
