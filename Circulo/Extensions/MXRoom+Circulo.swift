//
//  MXRoom+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 24.09.24.
//

import Foundation
import MatrixSDK

extension MXRoom {

    var liveTimeline: MXEventTimeline? {
        get async {
            await withCheckedContinuation { continuation in
                liveTimeline { timeline in
                    continuation.resume(returning: timeline)
                }
            }
        }
    }
}
