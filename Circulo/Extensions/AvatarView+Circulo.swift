//
//  AvatarView+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 23.02.24.
//

import KeanuCore

extension AvatarView {

    func load(_ member: Member?) {
        guard let member = member else {
            image = nil
            return
        }

        load(session: Circles.shared.session, id: member.id,
             avatarUrl: member.mxMember.avatarUrl, displayName: member.displayName)
    }
}
