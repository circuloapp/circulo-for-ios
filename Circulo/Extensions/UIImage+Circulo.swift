//
//  UIImage+Circulo.swift
//  Circulo
//
//  Created by N-Pex on 01.09.21.
//

import UIKit

extension UIImage {

    static let send = UIImage(named: "ic_send")
    static let resolve = UIImage(named: "ic_resolve")
    static let editDetails = UIImage(named: "ic_edit_details")
    static let shareLink = UIImage(named: "ic_share_link")
    static let qr = UIImage(named: "ic_qr")
    static let leave = UIImage(named: "ic_leave")
    static let delete = UIImage(named: "ic_delete")
    static let feedback = UIImage(named: "ic_feedback")
    static let about = UIImage(named: "ic_about")
    static let account = UIImage(named: "ic_account")
    static let settings = UIImage(named: "ic_settings")
    static let notifications = UIImage(named: "ic_notifications")
    static let notifyNew = UIImage(named: "ic_notify_new")
    static let radioOn = UIImage(named: "ic_radio_on")
    static let radioOff = UIImage(named: "ic_radio_off")
    static let notifyUpdate = UIImage(named: "ic_notify_update")
    static let notifyNormal = UIImage(named: "ic_notify_normal")
    static let notifyUrgent = UIImage(named: "ic_notify_urgent")
    static let back = UIImage(named: "ic_back")
    static let tabBackground = UIImage(named: "tab_background")

    static let noNetwork = UIImage(systemName: "icloud.slash.fill")

    class func avatar(_ index: Int) -> UIImage? {
        UIImage(named: "Circulo Avatars_2-\(index)")
    }

    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)

        color.setFill()

        let width = size.width
        let startPadding = 0.2 * width

        UIRectFill(CGRect(x: startPadding, y: 0, width: width - 2 * startPadding, height: lineWidth))

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
}
