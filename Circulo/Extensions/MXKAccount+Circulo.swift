//
//  MXKAccount+Circulo.swift
//  Circulo
//
//  Created by N-Pex on 24.11.21.
//

import KeanuCore
import MatrixSDK
import Foundation

extension MXKAccount {
    /**
     This is basically a hack to enable a notification service extension (NSE) to
     handle incoming push messages. We do this by appending the "mutable-content"
     flag to the parameters that get sent to the push server (via Matrix).
     */
    public class func enableNotificationServiceExtension() {
        swizzle(self, originalSelector: Selector(("enablePusher:appId:token:pushData:success:failure:")), swizzledSelector: #selector(overridden_enablePusher(enable:appId:token:pushData:success:failure:)))
    }
    
    @objc func overridden_enablePusher(enable: Bool, appId: String, token: Data, pushData: [String: Any], success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        // Modify the default payload!
        var data = pushData
        let defaultPayload = ["aps": ["mutable-content":1, "alert": ["body": "Incoming message"]] as [String : Any]]
        data["default_payload"] = defaultPayload
        overridden_enablePusher(enable: enable, appId: appId, token: token, pushData: data, success: success, failure: failure)
    }
    
    fileprivate class func swizzle(_ clazz:AnyClass, originalSelector:Selector, swizzledSelector:Selector) -> Void {        
        if let originalMethod = class_getInstanceMethod(clazz, originalSelector),
           let swizzledMethod = class_getInstanceMethod(clazz, swizzledSelector) {
            
            let didAddMethod = class_addMethod(clazz, originalSelector, method_getImplementation(swizzledMethod), method_getTypeEncoding(swizzledMethod))
            
            if didAddMethod {
                class_replaceMethod(clazz, swizzledSelector, method_getImplementation(originalMethod), method_getTypeEncoding(originalMethod))
            } else {
                method_exchangeImplementations(originalMethod, swizzledMethod)
            }
        }
        
    }
    
}
