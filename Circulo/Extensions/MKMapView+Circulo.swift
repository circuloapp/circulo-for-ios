//
//  MKMapView+Circulo.swift
//  Circulo
//
//  Created by Benjamin Erhart on 12.04.24.
//

import MapKit
import MatrixSDK

extension MKMapView: @retroactive MKMapViewDelegate {

    func addPolyline(_ coordinates: [CLLocationCoordinate2D]) -> MKPolyline {
        delegate = self

        let overlay = MKPolyline(coordinates: coordinates, count: coordinates.count)

        addOverlay(overlay)

        return overlay
    }

    func update(track: [MXEvent], old overlays: [MKOverlay]) -> [MKOverlay] {
        if let current = track.last?.location?.location2d,
           annotations.filter({ $0 is CirculoAnnotation })
            .contains(where: { $0.coordinate.latitude == current.latitude && $0.coordinate.longitude == current.longitude })
        {
            // Don't update anything which wasn't changed, so it doesn't flicker.
            return overlays
        }

        for overlay in overlays {
            removeOverlay(overlay)
        }

        removeAnnotations(annotations.filter({ $0 is CirculoAnnotation }))

        var overlays = [MKOverlay]()
        var last: MXEvent? = nil
        var polyline = [CLLocationCoordinate2D]()

        for event in track {
            // Split lines, when events are more than 5 minutes apart.
            if let last = last, event.originServerTs.timeInterval > last.originServerTs.timeInterval + 15 * 60 {
                overlays.append(addPolyline(polyline))

                polyline.removeAll()
            }

            if let location = event.location?.location2d {
                polyline.append(location)
            }

            last = event
        }

        if !polyline.isEmpty {
            overlays.append(addPolyline(polyline))

            addAnnotation(CirculoAnnotation(event: track.last!))
        }

        return overlays
    }

    func setRegion(_ center: CLLocationCoordinate2D, size: CLLocationDistance, animated: Bool = true) {
        setRegion(
            MKCoordinateRegion(center: center, latitudinalMeters: size, longitudinalMeters: size),
            animated: animated)
    }


    // MARK: MKMapViewDelegate

    public func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolyline {
            let renderer = MKPolylineRenderer(overlay: overlay)
            renderer.strokeColor = .accentDark

            return renderer
        }

        return MKOverlayRenderer(overlay: overlay)
    }
}
