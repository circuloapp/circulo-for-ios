//
//  JoinCircleViewController.swift
//  Circulo
//
//  Created by N-Pex on 27.7.21.
//

import Keanu
import KeanuCore
import MaterialComponents.MaterialSnackbar

public protocol JoinCircleViewControllerDelegate: AnyObject {
    func joinedRoom(id: String)
}

class JoinCircleViewController: BaseViewController, UITextFieldDelegate, CreateCircleViewControllerDelegate {

    weak var delegate : JoinCircleViewControllerDelegate?

    @IBOutlet weak var buttonDone: UIBarButtonItem!
    @IBOutlet weak var editCircle: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonDone.isEnabled = false
        editCircle.delegate = self
    }
    
    // Allow only paste, not typing!
    // Adapted from: https://newbedev.com/how-to-know-when-text-is-pasted-into-uitextview
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if string.count > 1{
                // Here we check if the replacement text is equal to the string we are currently holding in the paste board
                if (string == UIPasteboard.general.string) {
                    //User did copy & paste
                    return true
                }
            }
        //User did input by keypad
        return false
     }

    @IBAction func didEdit(_ sender: Any) {
        buttonDone.isEnabled = (editCircle.text?.count ?? 0) > 0
    }
    
    @IBAction func didPressDone(sender: Any) {
        if let circle = editCircle?.text, circle.count > 0 {
            joinCircle(idString: circle)
        }
    }
    
    func joinCircle(idString: String) {
        // Room id or invite link?
        // Note - Simplified parsing. Assume %@ (i.e. the user id) is always at the end of the invite url.
        var id = idString

        if !MXTools.isMatrixRoomIdentifier(idString) 
            && !MXTools.isMatrixRoomAlias(idString),
           let index = Config.inviteLinkFormat.range(of: "%@")
        {
            let prefix = Config.inviteLinkFormat[..<index.lowerBound]
            if idString.hasPrefix(prefix) {
                id = String(idString[index.lowerBound...])
            }
        }

        guard MXTools.isMatrixRoomIdentifier(id) || MXTools.isMatrixRoomAlias(id) else {
            let message = MDCSnackbarMessage()
            message.duration = 3
            message.text = "Invalid invite link format".localize()
            MDCSnackbarManager.default.show(message)
            return
        }

        workingOverlay.isHidden = false
        buttonDone.isEnabled = false

        Task {
            do {
                let room = try await Circles.shared.join(roomIdOrAlias: id)

                await MainActor.run {
                    workingOverlay.isHidden = true
                    buttonDone.isEnabled = true

                    delegate?.joinedRoom(id: room.roomId)
                }
            }
            catch {
                await MainActor.run {
                    workingOverlay.isHidden = true
                    buttonDone.isEnabled = true

                    let message = MDCSnackbarMessage()
                    message.duration = 3
                    message.text = error.localizedDescription
                    MDCSnackbarManager.default.show(message)
                }
            }
        }
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CreateCircleViewController {
            vc.delegate = self
        }
    }
    
    func onRoomCreated(room: MXRoom) {
        delegate?.joinedRoom(id: room.roomId)
    }
    
    override open func keyboardWillShow(notification: Notification) {
        if let kbSize = getKeyboardSize(notification) {
            self.bottomLayoutConstraint.constant = kbSize.height
            animateDuringKeyboardMovement(notification)
        }
    }
    
    override open func keyboardWillBeHidden(notification: Notification) {
        bottomLayoutConstraint.constant = 0
        animateDuringKeyboardMovement(notification)
    }
}
