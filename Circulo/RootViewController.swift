//
//  RootViewController.swift
//  Circulo
//
//  Created by N-Pex on 26.04.21.
//

import Keanu

class RootViewController: MainViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeNavigationViewController")

        viewControllers = [
            vc
        ]
    }
    
    override func showChatList() -> UINavigationController? {
        super.show(HomeViewController.self)
    }
}
