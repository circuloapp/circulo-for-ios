//
//  LocationTrack.swift
//  Circulo
//
//  Created by Benjamin Erhart on 13.05.24.
//

import Foundation
import MatrixSDK

class LocationTrack: Comparable {

    // MARK: Comparable

    @MainActor
    static func < (lhs: LocationTrack, rhs: LocationTrack) -> Bool {
        lhs.start ?? 0 < rhs.start ?? 0
    }


    // MARK: Equatable

    @MainActor
    static func == (lhs: LocationTrack, rhs: LocationTrack) -> Bool {
        lhs.events.first == rhs.events.first
    }


    // MARK: Public Properties

    @MainActor
    var threadId: String {
        events.first?.threadId ?? events.first?.eventId ?? ""
    }

    /**
     Timestamp in ms since epoch
     */
    @MainActor
    var start: UInt64? {
        events.first?.originServerTs
    }

    /**
     Timestamp in ms since epoch
     */
    private(set) var end: UInt64?

    /**
     Timestamp in ms since epoch
     */
    @MainActor
    var updated: UInt64? {
        events.last?.originServerTs
    }

    @MainActor
    var progress: Float {
        guard let start = start?.timeInterval,
              let end = end?.timeInterval
        else {
            return 0
        }

        return Float((Date.now.timeIntervalSince1970 - start) / (end - start))
    }

    var left: TimeInterval {
        guard let end = end?.timeInterval else {
            return 0
        }

        return max(0, end - Date.now.timeIntervalSince1970)
    }


    @MainActor
    private(set) var events: [MXEvent]

    @MainActor
    var locations: [MXEvent] {
        events.filter({ $0.location != nil})
    }

    private(set) var reactions = [MXEvent]()

    var haveRecognized: Bool {
        reactions.contains { $0.sender == Circles.shared.myUserId }
    }


    @MainActor
    init(_ event: MXEvent) {
        events = [event]
    }


    // MARK: Public Methods

    /**
     Add a given event.

     Only adds, if
     - it's not redacted,
     - it belongs to this thread, contains a location and was sent by this user,
     - it is a reaction and relates to the thread root event,
     - it belongs to this thread, contains a ISO 8601 date and was sent by this user.

     - parameter event: An `MXEvent` for potential addition..
     - returns: `true` if anything changed.
     */
    @discardableResult
    func add(event: MXEvent) async -> Bool {
        guard !event.isRedactedEvent() else {
            return false
        }

        let threadId = await self.threadId

        if event.eventType == .reaction && event.relatesTo?.eventId == threadId {
            if !reactions.contains(event) {
                reactions.append(event)

                return true
            }

            return false
        }

        let sender = await events.first?.sender

        if event.threadId == threadId && event.sender == sender {
            if await !events.contains(event) {
                await append(event: event)

                if event.containsLocation {
                    return true
                }

                let sender = await events.first?.sender

                if !event.containsLocation
                    && event.eventType == .roomMessage,
                   let message = event.rawMessageBody,
                   !message.isEmpty
                    && event.sender == sender
                {
                    let df = ISO8601DateFormatter()

                    if let date = df.date(from: message)?.msSinceEpoch {
                        if end == nil || end! < date {
                            end = date

                            return true
                        }
                    }
                }
            }
        }

        return false
    }

    /**
     React to root event, to indicate, that the user recignized the location track.

     Only possible on threads **not owened** by the user.
     */
    func recognize() async throws {
        guard let event = await events.first,
              event.sender != Circles.shared.myUserId
        else {
            return
        }

        try await Circles.shared.react(to: threadId, in: event.roomId)
    }

    /**
     Load the location tracking thread using the thread API and load all reactions to the thread root event.
     */
    func load() async throws {
        for event in try await Circles.shared.loadThread(threadId) {
            await add(event: event)
        }

        guard let event = await events.first else {
            return
        }

        reactions = try await Circles.shared.getReactions(event)
    }

    /**
     Try to redact all events which belong to this location track.
     */
    func redact() async {
        // Fix: Redact from first to last. This way, the chances are higher, the thread starter is
        // redacted, hence the thread ignored in the future, even if the server
        // gets in the way with "too many requests" errors.
        await redact(events: events)

        // Seriously remove everything.
        for event in (try? await Circles.shared.loadThread(threadId)) ?? [] {
            if !event.isRedactedEvent() {
                do {
                    try await Circles.shared.redact(event)

                    await remove(eventId: event.eventId)
                }
                catch {
                    // Ignore
                }
            }
        }
    }

    /**
     Only redact the end time of the last location thread, so location sharing stops,
     but others still can see my location until I finally redact all of it.
     */
    func redactEndTime() async {
        await redact(events: events.filter({ !$0.containsLocation }))

        end = Date.now.msSinceEpoch
    }

    /**
     Remove an event from the track list.

     - returns: `true` if anything changed.
     */
    func removeEvent(with eventId: String) async -> Bool {
        let before = await events.count

        await remove(eventId: eventId)

        return await events.count != before
    }


    // MARK: Private Methods

    private func redact(events: [MXEvent]) async {
        var toDelete = [String]()

        for event in events {
            do {
                try await Circles.shared.redact(event)

                toDelete.append(event.eventId)
            }
            catch {
                // Ignored
            }
        }

        for eventId in toDelete {
            await remove(eventId: eventId)
        }
    }

    @MainActor 
    private func remove(eventId: String) {
        events.removeAll { $0.eventId == eventId }
    }

    @MainActor
    private func append(event: MXEvent) {
        events.append(event)
        events.sort { $0.originServerTs <= $1.originServerTs }
    }
}
