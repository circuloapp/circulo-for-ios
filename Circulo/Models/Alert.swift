//
//  Alert.swift
//  Circulo
//
//  Created by Benjamin Erhart on 14.05.24.
//

import Foundation
import MatrixSDK

class Alert: Comparable {

    // MARK: Comparable

    static func < (lhs: Alert, rhs: Alert) -> Bool {
        lhs.event.originServerTs < rhs.event.originServerTs
    }


    // MARK: Equatable

    static func == (lhs: Alert, rhs: Alert) -> Bool {
        lhs.event.eventId == rhs.event.eventId
    }


    var threadId: String {
        event.eventId
    }

    /**
     The timestamp in ms since Epoch when this alert was created.
     */
    var timestamp: UInt64 {
        event.originServerTs
    }

    @MainActor
    var resolved: Bool {
        reactions.contains { $0.sender == event.sender }
    }

    /**
     The timestamp in ms since Epoch when this alert was marked resolved.
     */
    @MainActor
    var resolvedTime: UInt64? {
        reactions.first(where: { $0.sender == event.sender })?.originServerTs
    }

    @MainActor
    var haveRecognized: Bool {
        reactions.contains { $0.sender == Circles.shared.myUserId }
    }

    @MainActor
    var countRecognized: Int {
        reactions.filter { $0.sender != event.sender }.count
    }


    let event: MXEvent

    private(set) var audio: Data?
    private(set) var audioError: Error?

    @MainActor
    private(set) var reactions = [MXEvent]()


    init(_ event: MXEvent) {
        self.event = event
    }


    // MARK: Public Methods

    /**
     Add a given event.

     Only adds, if
     - it's not redacted,
     - it belongs to this thread, contains an audio attachment and was sent by this user,
     - it is a reaction and relates to the thread root event,

     - parameter event: An `MXEvent` for potential addition..
     - returns: `true` if anything changed.
     */
    @discardableResult
    func add(event: MXEvent) async -> Bool {
        guard !event.isRedactedEvent() else {
            return false
        }

        if event.threadId == threadId && event.isAudioAttachment() && event.sender == event.sender {

#if canImport(Keanu)
            if audio == nil {
                Task {
                    do {
                        audio = try await Circles.shared.loadAttachment(event)
                        audioError = nil
                    }
                    catch {
                        audio = nil
                        audioError = error
                    }
                }
            }
#endif

            return true
        }

        if event.eventType == .reaction && event.relatesTo?.eventId == threadId {
            if await !reactions.contains(event) {
                await MainActor.run {
                    reactions.append(event)
                }

                return true
            }
        }

        return false
    }

    /**
     React to root event, to indicate, that the alert was resolved.

     Only possible on threads **owned** by the user.
     */
    func resolve() async throws {
        guard event.sender == Circles.shared.myUserId else {
            return
        }

        try await Circles.shared.react(with: "✅", to: event)
    }

    /**
     React to root event, to indicate, that the user recignized the alert.

     Only possible on threads **not owened** by the user.
     */
    func recognize() async throws {
        guard event.sender != Circles.shared.myUserId else {
            return
        }

        try await Circles.shared.react(to: event)
    }

    /**
     Load the alert thread using the thread API and load all reactions to the thread root event.
     */
    func load() async throws {
        Task { @MainActor in
            reactions = try await Circles.shared.getReactions(event)
        }

        for event in try await Circles.shared.loadThread(event.eventId) {
            await add(event: event)
        }
    }
}
