//
//  Member.swift
//  Circulo
//
//  Created by Benjamin Erhart on 13.05.24.
//

import Foundation
import MatrixSDK
import KeanuCore
import AFNetworking

class Member: Hashable, Comparable {

    enum Issue {

        /**
         This user has a resolved alert, or an alert which was resolved less than 3 hours ago.
         */
        case alert

        /**
         This user has an ongoing location tracking.
         */
        case tracking
    }

    // MARK: Comparable

    @MainActor
    static func < (lhs: Member, rhs: Member) -> Bool {
        if lhs.isMe {
            return true
        }

        if rhs.isMe {
            return false
        }

        if lhs.hasUnresolvedAlert && !rhs.hasUnresolvedAlert {
            return true
        }

        if !lhs.hasUnresolvedAlert && rhs.hasUnresolvedAlert {
            return false
        }

        if let a1 = lhs.lastAlert, let a2 = rhs.lastAlert {
            return a1.timestamp >= a2.timestamp
        }

        return lhs.id >= rhs.id
    }


    // MARK: Equatable

    static func == (lhs: Member, rhs: Member) -> Bool {
        lhs.id == rhs.id
    }

    // MARK: Public Properties

    var id: String {
        mxMember.userId ?? ""
    }

    var displayName: String {
        mxMember.displayname ?? id
    }

    var isMe: Bool {
        id == Circles.shared.myUserId
    }

    /**
     `true` if we found at least the original join or invite event or a leave event for this member.
     */
    var isInitialized: Bool {
        originalJoin != nil || invite != nil || leave != nil
    }

    /**
     If `true`, member should be considered to be in the circle, currently. If `false`, member left again.
     */
    var isCurrent: Bool {
        originalJoin?.originServerTs ?? invite?.originServerTs ?? 1 > leave?.originServerTs ?? 0
    }

    @MainActor
    var locationTrack: LocationTrack? {
        locationTracks.last
    }

    private(set) var mxMember: MXRoomMember

    private(set) var invite: MXEvent?

    private(set) var originalJoin: MXEvent?

    private(set) var lastJoin: MXEvent?

    private(set) var leave: MXEvent?

    private(set) var lastAlert: Alert?

    @MainActor
    var hasUnresolvedAlert: Bool {
        !(lastAlert?.resolved ?? true)
    }

    @MainActor
    var haveRecognizedAlert: Bool {
        lastAlert?.haveRecognized ?? true
    }

    @MainActor
    var haveRecognizedTracking: Bool {
        locationTrack?.haveRecognized ?? true
    }

    @MainActor
    var haveNotRecognizedAll: Bool {
        !haveRecognizedAlert || !haveRecognizedTracking
    }

    @MainActor
    var hasIssues: [Issue] {
        var issues = [Issue]()

        if let alert = lastAlert {
            if let resolvedTime = alert.resolvedTime?.date {
                let threeHoursAgo = Calendar.current.date(byAdding: .hour, value: -3, to: .now) ?? Date.now

                if resolvedTime > threeHoursAgo {
                    // Resolved less than 3 hours ago.
                    issues.append(.alert)
                }
            }
            else {
                // Unresolved alert.
                issues.append(.alert)
            }
        }

        if let locationTrack = locationTrack {
            if issues.first == .alert {
                if let resolvedTime = lastAlert?.resolvedTime?.date, let startTime = locationTrack.start?.date {
                    if startTime > resolvedTime {
                        // There is a location track, but it's younger than the alert.
                        // Show both seperately!
                        issues.append(.tracking)
                    }
                }
            }
            else {
                issues.append(.tracking)
            }
        }

        return issues
    }


    // MARK: Private Properties

    @MainActor
    private var locationTracks = [LocationTrack]()


    init(_ mxMember: MXRoomMember) {
        self.mxMember = mxMember

        guard let event = mxMember.originalEvent else {
            return
        }

        if event.isOriginalJoin {
            originalJoin = event
        }
        else if event.isJoin {
            lastJoin = event
        }
        else if event.isInvite {
            invite = event
        }
        else if event.isLeave {
            leave = event
        }

        // Update FriendsManager, so RoomViewController shows proper names.
        Task { @MainActor in
            FriendsManager.shared.add(Friend(mxMember))
        }
    }

    /**
     - parameter event: **Must** be an event of type `.roomMember`, otherwise the underlying `MXRoomMember.init` call will crash !
     */
    convenience init(_ event: MXEvent) {
        self.init(MXRoomMember(mxEvent: event))
    }


    // MARK: Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }


    // MARK: Public Methods

    /**
     Add a given event.

     - If this is a redacted event, check all location events and remove, where IDs match.
     - If this is an original `join`, `invite` or `leave` event, add it to the `Member` itself, if it's newer than what we already have.
     - If this is a location event, add it to the location tracks.
     - If this is a root alert thread event, set it, if it's newer than what we already got.
     - If this is an event of an alert thread, add it to the one we got.
     - If this is a reaction, check the root events of location tracks and the alert and add it, if found.

     - parameter event: An `MXEvent` for potential addition..
     - parameter load: If `true`, will trigger a full load of newly identified alerts and locations. You will be responsible for triggering that, whenever you're done, if you set this to `false`.
     - returns: `true` if anything changed.
     */
    @discardableResult
    func add(event: MXEvent, load: Bool = true) async throws -> Bool {
        if event.isRedactedEvent() || event.eventType == .roomRedaction {
            guard let id = event.redacts ?? event.eventId else {
                return false
            }

            if let track = await getFirstTrack(withThread: id, orEvent: true) {
                if await track.threadId == id {
                    await remove(track: track)

                    return true
                }
                else {
                    return await track.removeEvent(with: id)
                }
            }

            return false
        }

        if event.isInThread() {
            if let track = await getFirstTrack(withThread: event.threadId) {
                return await track.add(event: event)
            }

            if lastAlert?.threadId == event.threadId {
                return await lastAlert?.add(event: event) ?? false
            }

            // Seems to be a tracking event for which we don't have the root event, yet.
            if event.containsLocation && event.sender == id {
                let track = await LocationTrack(event)

                await MainActor.run {
                    locationTracks.append(track)
                    locationTracks.sort()
                }

                if load {
                    Task {
                        try await track.load()
                    }
                }

                return true
            }

            return false
        }

        if event.isOriginalJoin && event.affectedUser == id {
            if originalJoin?.originServerTs ?? 0 < event.originServerTs {
                originalJoin = event

                return true
            }

            return false
        }

        if event.isInvite && event.affectedUser == id {
            if invite?.originServerTs ?? 0 < event.originServerTs {
                invite = event

                return true
            }

            return false
        }

        if event.isLeave && event.affectedUser == id {
            if leave?.originServerTs ?? 0 < event.originServerTs && originalJoin?.originServerTs ?? 0 < event.originServerTs {
                leave = event

                return true
            }

            return false
        }

        // User changed their profile. Use latest join event to resolve name.
        if event.isJoin && event.affectedUser == id {
            if lastJoin?.originServerTs ?? 0 < event.originServerTs {
                mxMember = MXRoomMember(mxEvent: event)
                lastJoin = event

                // Update FriendsManager, so RoomViewController shows proper names.
                Task { @MainActor in
                    FriendsManager.shared.add(Friend(mxMember))
                }

                return true
            }

            return false
        }

        // A root alert event.
        if event.isUrgent && event.sender == id {
            let alert = Alert(event)

            if lastAlert == nil || lastAlert! < alert {
                lastAlert = alert

                if load {
                    Task {
                        try await alert.load()
                    }
                }

                return true
            }

            return false
        }

        // A root tracking event, which we don't have, yet.
        if event.containsLocation && event.sender == id {
            // Maybe we cought one of its thread events, already?
            if let track = await getFirstTrack(withThread: event.eventId) {
                return await track.add(event: event)
            }
            else {
                let track = await LocationTrack(event)

                await MainActor.run {
                    locationTracks.append(track)
                    locationTracks.sort()
                }

                if load {
                    Task {
                        try await track.load()
                    }
                }

                return true
            }
        }

        if event.eventType == .reaction, let threadId = event.relatesTo?.eventId {
            if let track = await getFirstTrack(withThread: threadId) {
                return await track.add(event: event)
            }
            else if lastAlert?.threadId == threadId {
                return await lastAlert?.add(event: event) ?? false
            }
        }

        return false
    }

    /**
     Load last location track and last alert using the thread API and load all reactions to the resp. thread root events.
     */
    func load() async throws {
        try await lastAlert?.load()

        try await locationTrack?.load()
    }


    /**
     Redact all current location tracks in the background on a best-effort basis.
     */
    func redactMyLocations() async {
        guard isMe else {
            return
        }

        // Move tracks to a new array, so sync code can continue without a hickup.
        let tracks = await locationTracks.map { $0 }

        await MainActor.run {
            locationTracks = []
        }

        for track in tracks {
            await track.redact()
        }
    }

    @MainActor
    func getLastLocation() -> MXEvent? {
        return locationTracks
            .map({ $0.events })
            .joined()
            .sorted(by: { $0.originServerTs <= $1.originServerTs })
            .last(where: { $0.containsLocation })
    }

    func addLocation(_ location: CLLocation, with name: String, until end: Date) async throws {
        guard let room = await Circles.shared.room(of: self) else {
            return
        }

        let threadId = await locationTrack?.threadId

        let eventId = try await Circles.shared.send(location: location, with: name, to: room, replyingTo: threadId)

        if threadId == nil {
            let df = ISO8601DateFormatter()

            try await Circles.shared.send(text: df.string(from: end), to: room, replyingTo: eventId)
        }
    }

    func alert(audioFile: URL?) async throws {
        guard await !hasUnresolvedAlert,
              let room = await Circles.shared.room(of: self)
        else {
            return
        }

        let text = "#urgent #notsafe"

#if canImport(Keanu)
        CleanInsightsManager.shared.measure(event: "condition", name: text, value: 0)

        if !AFNetworkReachabilityManager.shared().isReachable {
            CleanInsightsManager.shared.measure(event: "offline-send")
        }
#endif

        let event = try await Circles.shared.send(text: text, to: room)

        if let audioFile = audioFile {
            try await Circles.shared.send(audio: audioFile, to: room, replyingTo: event.eventId)
        }
    }

    @MainActor
    func hasThread(with id: String) -> Bool {
        lastAlert?.threadId == id || locationTracks.contains(where: { $0.threadId == id })
    }


    // MARK: Private Methods

    @MainActor
    private func getFirstTrack(withThread id: String?, orEvent: Bool = false) -> LocationTrack? {
        if orEvent {
            return locationTracks.first(where: { $0.threadId == id || $0.events.contains(where: { $0.eventId == id }) })
        }
        else {
            return locationTracks.first { $0.threadId == id }
        }
    }

    @MainActor
    private func remove(track: LocationTrack) {
        locationTracks.removeAll { $0 == track }
    }
}
