//
//  AlertViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 23.02.24.
//

import UIKit
import KeanuCore
import Keanu

class AlertViewController: UIViewController, UICollectionViewDataSource,
                            UICollectionViewDelegateFlowLayout, AudioPlayerDelegate
{
    var member: Member?


    @IBOutlet weak var alertContainer: UIView!

    @IBOutlet weak var alertTitleLb: UILabel! {
        didSet {
            alertTitleLb.text = "Alert Active".localize()
        }
    }

    @IBOutlet weak var seenLb: UILabel! {
        didSet {
            seenLb.text = "Waiting to be seen…".localize()
        }
    }

    @IBOutlet weak var seenCollection: UICollectionView!
    @IBOutlet weak var seenCollectionHeight: NSLayoutConstraint!

    @IBOutlet weak var okayBt: UIButton! {
        didSet {
            okayBt.setTitle("I'm Okay".localize())
        }
    }

    @IBOutlet weak var sentLb: UILabel!
    @IBOutlet weak var locationLb: UILabel!
    @IBOutlet weak var playAudioBt: UIButton! {
        didSet {
            playAudioBt.imageView?.tintColor = .accent
        }
    }


    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    private var membersSeen = [Member]()

    private var player: AudioPlayer?


    override func viewDidLoad() {
        super.viewDidLoad()

        isModalInPresentation = true

        if let sheet = sheetPresentationController {
            sheet.detents = [.medium(), .large()]
            sheet.prefersGrabberVisible = true
            sheet.preferredCornerRadius = 16
        }

        seenCollection.register(SeenCell.nib, forCellWithReuseIdentifier: SeenCell.identifier)

        update()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(memberChanged), name: .memberChanged, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        player?.stop()

        NotificationCenter.default.removeObserver(self)
    }


    // MARK: Actions

    @IBAction func okay() {
        guard let member = member,
              let alert = member.lastAlert
        else {
            return
        }

        AlertHelper.present(
            self,
            message: "We'll notify your circle and resolve the alert.".localize(),
            title: "Glad you're okay".localize(),
            actions: [
                AlertHelper.cancelAction("Not okay".localize()),
                AlertHelper.destructiveAction("Okay", handler: { [weak self] _ in
                    self?.workingOverlay.isHidden = false

                    Task {
                        do {
                            try await alert.resolve()

                            let in10Months = Calendar.current.date(byAdding: .month, value: 10, to: .now)

                            let end = member.locationTrack?.end?.date

                            // It's implausible that somebody tries to track themselves for
                            // more than a few days, let alone 6 month.
                            // Since tracking during an alert is set to go on for 1 year,
                            // we assume that every value bigger than 6 month
                            // away means, that this was tracking triggered through an alert.
                            // Therefore, since the alert is over, we also stop the tracking again
                            // and redact the end time, so location sharing stops.

                            if end == nil || in10Months == nil || end! > in10Months! {
                                LocationManager.shared.cancel()

                                await member.locationTrack?.redactEndTime()
                            }
                            else if let end = end {
                                LocationManager.shared.trackLocation(.until(arrival: end))
                            }

                            let resolveTime = Double(Date.now.msSinceEpoch - alert.timestamp)
                            CleanInsightsManager.shared.measure(event: "status-resolved", name: "resolve-time", value: resolveTime)

                            await MainActor.run {
                                self?.dismiss(animated: true)
                            }
                        }
                        catch {
                            if let self = self {
                                await MainActor.run {
                                    self.workingOverlay.isHidden = true

                                    AlertHelper.present(self, message: error.localizedDescription)
                                }
                            }
                        }
                    }
                })
            ])
    }

    @IBAction func playAudio() {
        if let error = member?.lastAlert?.audioError {
            AlertHelper.present(self, message: error.localizedDescription)

            return
        }

        if player?.isPlaying ?? false {
            playAudioBt.setImage(.init(systemName: "play.fill"))

            player?.stop()
        }
        else {
            playAudioBt.setImage(.init(systemName: "stop.fill"))

            // Stupid workaround for MatrixSDKs wild errors about being unable to decrypt
            // the audio file from the audio attachment replay event we just sent.
            // So, fall back to play the last recorded audio instead, so the user
            // hears something.
            player?.play(fallback: CriticalAlertViewController.audioFile)
        }
    }


    @objc
    func memberChanged(_ notification: Notification) {
        guard let member = notification.object as? Member,
              member.isMe
        else {
            return
        }

        self.member = member

        update()
    }

    func update() {
        updateReaders(member?.lastAlert?.reactions.compactMap({ Circles.shared.member(with: $0.sender) }) ?? [])

        let ts = member?.lastAlert?.timestamp.date ?? .init(timeIntervalSince1970: 0)
        sentLb.text = "Sent %".localize(value: Formatters.format(friendlyTimestamp: ts))

        if let location = member?.getLastLocation()?.location {
            if let name = location.locationDescription, !name.isEmpty {
                setLocation(name)
            }
            else {
                Task { [weak self] in
                    let name = await LocationManager.geocode(location.location)

                    await MainActor.run {
                        self?.setLocation(name)
                    }
                }
            }
        }
        else {
            setLocation("Unknown".localize())
        }

        if let alert = member?.lastAlert, Settings.allowAccessToMicrophone {
            player = AudioPlayer(alert)
            player?.delegate = self

            playAudioBt.isHidden = false
        }
        else {
            playAudioBt.isHidden = true
        }
    }


    // MARK: UICollectionViewDataSource

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        membersSeen.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SeenCell.identifier, for: indexPath)

        cell.layer.cornerRadius = 8
        cell.layer.masksToBounds = true

        return (cell as? SeenCell)?.set(membersSeen[indexPath.row]) ?? cell
    }


    // MARK: UICollectionViewDelegateFlowLayout

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, 
                        sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        CGSize(width: collectionView.bounds.width / 2 - 5, height: 40)
    }


    // MARK: AudioPlayerDelegate

    func audioPlayerDidFinish() {
        playAudioBt.setImage(.init(systemName: "play.fill"))
    }

    func audioPlayerError(_ error: any Error) {
        AlertHelper.present(self, message: error.localizedDescription)
    }

    func audioPlayerTimeUpdate(_ duration: TimeInterval, _ currentTime: TimeInterval) {
        // Ignored
    }


    // MARK: Private Methods

    private func updateReaders(_ readers: [Member]) {
        for reader in readers {
            guard !reader.isMe && !membersSeen.contains(reader) else {
                continue
            }

            membersSeen.append(reader)
        }

        if membersSeen.isEmpty {
            seenLb.text = "Waiting to be seen…".localize()
        }
        else {
            seenLb.text = "Seen by % people".localize(value: Formatters.format(int: membersSeen.count))
        }

        seenCollection.reloadData()

        seenCollectionHeight.constant = seenCollection.collectionViewLayout.collectionViewContentSize.height
        view.layoutIfNeeded()
    }

    private func setLocation(_ name: String) {
        let text = NSMutableAttributedString(string: "Location was %".localize(value: name))

        if let range = text.range(of: name),
           let boldFont = locationLb.font.bold
        {
            text.setAttributes([.font: boldFont], range: range)
        }

        locationLb.attributedText = text
    }
}
