//
//  CirculoAnnotationView.swift
//  Circulo
//
//  Created by Benjamin Erhart on 07.02.24.
//

import MapKit
import KeanuCore

class CirculoAnnotationView: MKAnnotationView {

    static weak var member: Member?

    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)

        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }


    private func setup() {
        frame = .init(x: 0, y: 0, width: 64, height: 73)
        centerOffset = CGPoint(x: 0, y: -frame.size.height / 2)

        canShowCallout = false

        let avatar = AvatarViewWithStatus(frame: .init(x: 0, y: 0, width: 64, height: 64))
        avatar.member = Self.member
        avatar.layer.borderColor = UIColor.pin.cgColor
        avatar.layer.borderWidth = 8
        avatar.layer.shadowOpacity = 0.75
        addSubview(avatar)

        let triangle = TriangleView(frame: .init(x: 24, y: 63, width: 16, height: 10))
        triangle.backgroundColor = .clear
        triangle.tintColor = .pin
        addSubview(triangle)
    }
}

class CirculoAnnotation: MKShape {

    private let _coordinate: CLLocationCoordinate2D

    override var coordinate: CLLocationCoordinate2D {
        _coordinate
    }

    init(event: MXEvent) {
        _coordinate = event.location?.location2d ?? .init()

        super.init()

        title = event.location?.locationDescription
        subtitle = Formatters.format(friendlyTimestamp: event.originServerTs.date)
    }
}
