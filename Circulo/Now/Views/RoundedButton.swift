//
//  RoundedButton.swift
//  Circulo
//
//  Created by N-Pex on 24.8.21.
//

import UIKit


open class RoundedButton: UIButton {
    open override func awakeFromNib() {
        super.awakeFromNib()
        autoSetDimension(.height, toSize: 48)
        makeRoundedAndShadowed(view: self)
    }
    
    func makeRoundedAndShadowed(view: UIView) {
        let shadowLayer = CAShapeLayer()
        
        view.layer.cornerRadius = 24
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds,
                                        cornerRadius: 32).cgPath
        shadowLayer.fillColor = view.backgroundColor?.cgColor
        shadowLayer.shadowColor = UIColor.label.cgColor
        shadowLayer.shadowOffset = CGSize(width: 0, height: 4.0)
        shadowLayer.shadowOpacity = 0.2
        shadowLayer.shadowRadius = 10.0
        view.layer.insertSublayer(shadowLayer, at: 0)
    }
}
