//
//  OtherLocationView.swift
//  Circulo
//
//  Created by Benjamin Erhart on 10.04.24.
//

import UIKit
import MapKit
import KeanuCore

class OtherLocationView: UIView {

    class var nib: UINib {
        UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    class func create() -> OtherLocationView {
        nib.instantiate(withOwner: nil).first as! OtherLocationView
    }


    @IBOutlet weak var map: MKMapView! {
        didSet {
            map.register(CirculoAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier:
                            MKMapViewDefaultAnnotationViewReuseIdentifier)
        }
    }

    @IBOutlet weak var avatar: AvatarView!
    @IBOutlet weak var locationLb: UILabel!

    @IBOutlet weak var progressContainer: UIView!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var leftLb: UILabel!


    private weak var member: Member?

    private var timer: Timer?


    func apply(_ member: Member?) {
        self.member = member

        avatar.load(member)

        CirculoAnnotationView.member = member

        timer?.invalidate()
        timer = .scheduledTimer(timeInterval: 5, target: self, selector: #selector(update), userInfo: nil, repeats: true)

        update()
    }

    @IBAction func tap() {
        let vc = CurrentLocationViewController()
        vc.member = member

        nextUiViewController?.navigationController?.pushViewController(vc, animated: true)
    }

    @objc
    private func update() {
        map.removeAnnotations(map.annotations)

        guard let track = member?.locationTrack,
              let location = track.locations.last
        else {
            timer?.invalidate()
            timer = nil

            locationLb.text = "Unknown Location".localize()
            progressContainer.isHidden = true

            return
        }

        let left = track.left

        if left <= 0 {
            timer?.invalidate()
            timer = nil
        }

        map.setRegion(location.location?.location2d ?? .init(), size: 800)
        map.addAnnotation(CirculoAnnotation(event: location))

        locationLb.text = location.location?.locationDescription

        progressContainer.isHidden = false

        progress.progress = track.progress

        if let value = Formatters.format(friendlyDuration: left) {
            leftLb.text = "% left".localize(value: value)
            leftLb.isHidden = false
        }
        else {
            leftLb.isHidden = true
        }
    }
}
