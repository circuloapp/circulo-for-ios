//
//  MyLocationView.swift
//  Circulo
//
//  Created by Benjamin Erhart on 06.02.24.
//

import UIKit
import MapKit
import KeanuCore


protocol MyLocationViewDelegate: UIViewController {
}

class MyLocationView: UIView, ShareLocationDelegate {

    class var nib: UINib {
        UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    static let shared: MyLocationView = {
        nib.instantiate(withOwner: nil, options: nil).first as! MyLocationView
    }()


    weak var delegate: MyLocationViewDelegate?


    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var locationLb: UILabel!
    @IBOutlet weak var durationLb: UILabel!
    @IBOutlet weak var deleteBt: UIButton! {
        didSet {
            // Annoying bug in Xcode or UIKit makes us do this.
            deleteBt.configuration?.baseBackgroundColor = .accent
        }
    }
    @IBOutlet weak var chevronIv: UIImageView?


    private var shareVc: ShareLocationViewController?
    private weak var currentVc: CurrentLocationViewController?


    func update() {
        let me = Circles.shared.me()

        // Make sure, location is tracked as long as an alert is ongoing.
        if (me?.hasUnresolvedAlert ?? false) && Settings.sendLocationWithAlerts && !LocationManager.shared.isTracking {
            LocationManager.shared.trackLocation(.year1)
        }

        if me?.locationTrack != nil || LocationManager.shared.isTracking  {
            isHidden = false

            let end = me?.locationTrack?.end?.date ?? .distantPast

            let duration = end.timeIntervalSince1970 - Date().timeIntervalSince1970

            if end > .now && !LocationManager.shared.isTracking {
                LocationManager.shared.trackLocation(.until(arrival: end))
            }

            let location = me?.locationTrack?.locations.last?.location

            if LocationManager.shared.isTracking {
                heightConstraint = 80

                if let location = location {
                    map.isHidden = false
                    map.setRegion(location.location2d, size: 800)
                    activityIndicator.isHidden = true
                    locationLb.text = location.locationDescription
                    chevronIv?.isHidden = false
                }
                else {
                    map.isHidden = true
                    activityIndicator.isHidden = false
                    locationLb.text = "Fetching Location".localize()
                    chevronIv?.isHidden = true
                }

                deleteBt.isHidden = true
                deleteBt.heightConstraint = 0

                if duration > 0 {
                    durationLb.text = "You are sharing for %".localize(value: Formatters.format(friendlyDuration: duration) ?? "–")
                }
                else {
                    durationLb.text = "You are sharing your location".localize()
                }
            }
            else {
                heightConstraint = 136

                map.isHidden = false
                if let location = location {
                    map.setRegion(location.location2d, size: 800)
                }

                activityIndicator.isHidden = true
                locationLb.text = "Live location off".localize()
                chevronIv?.isHidden = false
                deleteBt.isHidden = false
                deleteBt.heightConstraint = 35

                let ago = Formatters.format(friendlyDuration: Date().timeIntervalSince1970 - end.timeIntervalSince1970)

                if let ago = ago, end != .distantPast {
                    durationLb.text = "Your history from % ago can be seen by your circle.".localize(value: ago)
                }
                else {
                    durationLb.text = "Your history can be seen by your circle.".localize()
                }
            }
        }
        else {
            isHidden = true
            heightConstraint = 0

            if currentVc != nil {
                currentVc?.navigationController?.popViewController(animated: true)
                currentVc = nil
            }
        }
    }


    // MARK: Actions

    @IBAction func tap() {
        if isHidden {
            share()
        }
        else {
            openCurrentLocationVc()
        }
    }

    @discardableResult
    func openCurrentLocationVc() -> CurrentLocationViewController? {
        if Circles.shared.me()?.locationTrack?.locations.last != nil {
            let vc = currentVc ?? CurrentLocationViewController()
            currentVc = vc

            vc.member = Circles.shared.me()

            nextUiViewController?.navigationController?.pushViewController(vc, animated: true)

            return vc
        }

        return nil
    }

    @IBAction func share() {
        guard let vc = delegate else {
            return
        }

        checkAuthorization(LocationManager.shared.status) { [weak self] in
            if self?.shareVc == nil {
                self?.shareVc = ShareLocationViewController()
                self?.shareVc?.delegate = self
            }

            vc.present(self!.shareVc!, animated: true)
        }
    }

    @IBAction func deleteHistory() {
        if let vc = openCurrentLocationVc() {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                vc.stop(force: true)
            }
        }
    }


    // MARK: ShareLocationDelegate

    func done(duration: LocationManager.Duration?) {
        shareVc = nil

        if let duration = duration {
            isHidden = false

            map.isHidden = true
            activityIndicator.isHidden = false

            locationLb.text = "Fetching Location".localize()

            durationLb.text = "You are sharing for %".localize(value: Formatters.format(friendlyDuration: duration.length) ?? "–")

            chevronIv?.isHidden = true

            LocationManager.shared.trackLocation(duration)
        }
    }


    // MARK: Private Methods

    private func checkAuthorization(_ status: CLAuthorizationStatus, completed: @escaping () -> Void) {
        let cancelAction = AlertHelper.cancelAction()

        let settingsAction = AlertHelper.defaultAction("Settings".localize()) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }

        switch status {
        case .notDetermined:
            return LocationManager.shared.requestAuthorization { [weak self] status in
                self?.checkAuthorization(status, completed: completed)
            }

        case .restricted:
            guard let vc = delegate else {
                return
            }

            return AlertHelper.present(
                vc,
                message: "Sorry, you cannot use this feature, because you're not allowed to share your location.".localize(),
                actions: [cancelAction]
            )

        case .denied:
            guard let vc = delegate else {
                return
            }

            let title = "Turn On Location Services to Allow \"%\" Determine Your Location".localize(value: Bundle.main.displayName)

            let message = "In order for your circle to know where you are, % needs access to your location always.".localize(value: Bundle.main.displayName)
                + "\n\n"
                + "In Settings, tap Location > \"Always\" and leave the \"Precise\" option on.".localize()
                + " "
                + "This allows the app to get your location, even when it's running in the background.".localize()

            return AlertHelper.present(vc, message: message, title: title,
                                       actions: [cancelAction, settingsAction])

        case .authorizedAlways:
            return completed()

        case .authorizedWhenInUse, .authorized:
            return LocationManager.shared.requestAuthorization { [weak self] status in
                if status != .authorizedAlways {
                    guard let self = self, let vc = self.delegate else {
                        return
                    }

                    let title = "Allow \"%\" to also use your location when the app is running in the background?".localize(value: Bundle.main.displayName)

                    let message = "In order for your circle to know where you are, % needs access to your location always.".localize(value: Bundle.main.displayName)
                        + " "
                        + "This allows the app to get your location even when it's running in the background.".localize()
                        + "\n\n"
                        + "Your location is only accessed by % when you choose to share live location or when you send an alert.".localize(value: Bundle.main.displayName)
                        + "\n\n"
                        + "In Settings, tap Location > \"Always\" and leave the \"Precise\" option on.".localize()

                    AlertHelper.present(vc, message: message, title: title,
                                        actions: [
                                            AlertHelper.cancelAction("OK".localize()) { _ in
                                                completed()
                                            },
                                            settingsAction
                                        ])
                }
                else {
                    completed()
                }
            }

        @unknown default:
            guard let vc = delegate else {
                return
            }

            return AlertHelper.present(
                vc,
                message: "Sorry, you cannot use this feature, because location access is in a state which cannot be recognized.".localize(),
                actions: [cancelAction]
            )
        }
    }

}
