//
//  TriangleView.swift
//  Circulo
//
//  Created by Benjamin Erhart on 07.02.24.
//

import UIKit

class TriangleView: UIView {

    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        context.beginPath()
        context.move(to: .init(x: rect.minX, y: rect.minY))
        context.addLine(to: .init(x: rect.maxX, y: rect.minY))
        context.addLine(to: .init(x: rect.maxX / 2, y: rect.maxY))
        context.closePath()
        context.setFillColor(tintColor.cgColor)
        context.fillPath()
    }
}
