//
//  CriticalAlertViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 29.02.24.
//

import UIKit
import AVFoundation
import AFNetworking

protocol CriticalAlertDelegate: AnyObject {

    func criticalAlertDismissed()
}

class CriticalAlertViewController: UIViewController {

    static let audioFile = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("recording.m4a")


    weak var uiDelegate: CriticalAlertDelegate?

    @IBOutlet weak var microphoneImg: UIImageView!

    @IBOutlet weak var titleLb: UILabel! {
        didSet {
            titleLb.text = "Recording Audio".localize()
        }
    }

    @IBOutlet weak var durationLb: UILabel!

    @IBOutlet weak var dbMeter: UIView!
    @IBOutlet weak var dbMeterHeight: NSLayoutConstraint! {
        didSet {
            dbMeterMaxHeight = dbMeterHeight.constant
        }
    }

    @IBOutlet weak var releaseLb: UILabel! {
        didSet {
            releaseLb.text = "Release to Send".localize()
        }
    }

    @IBOutlet weak var sendBt: UIButton? {
        didSet {
            // Damnit. UIButton is sooo buggy.
            sendBt?.backgroundColor = .alertDark
        }
    }

    @IBOutlet weak var sendBtCenter: NSLayoutConstraint! {
        didSet {
            sendBtCenterConstant = sendBtCenter.constant
        }
    }
    @IBOutlet weak var sendBtBottom: NSLayoutConstraint! {
        didSet {
            sendBtBottomConstant = sendBtBottom.constant
        }
    }

    @IBOutlet weak var cancelBt: UIButton? {
        didSet {
            // Damnit. UIButton is sooo buggy.
            cancelBt?.imageView?.tintColor = .white
        }
    }

    @IBOutlet weak var sendingOverlay: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sentImg: UIImageView!
    @IBOutlet weak var statusLb: UILabel! {
        didSet {
            statusLb.text = "Sending…".localize()
        }
    }


    private var sendBtCenterConstant = CGFloat(0)
    private var sendBtBottomConstant = CGFloat(0)
    private var sendBtCenterPoint = CGPoint.zero
    private var appeared = false

    private var recorder: AVAudioRecorder?

    private var timeTimer: Timer?

    private var dbMeteringTimer: Timer?
    private var dbMeterMaxHeight: CGFloat = 0

    private lazy var durationFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()

        formatter.unitsStyle = .positional
        formatter.allowedUnits = [ .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]

        return formatter
    }()


    /**
     Haptic feedback (vibration)
     */
    private lazy var haptics: UINotificationFeedbackGenerator? = .init()


    override func viewDidLoad() {
        super.viewDidLoad()

        haptics?.notificationOccurred(.success)

        if Settings.allowAccessToMicrophone {
            updateTime()
            updateDbMeter()
        }
        else {
            microphoneImg.isHidden = true
            titleLb.isHidden = true
            durationLb.isHidden = true
            dbMeter.isHidden = true
        }
    }


    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        appeared = true

        // Don't record anything if user doesn't wish to!
        guard Settings.allowAccessToMicrophone else {
            return
        }

        let session = AVAudioSession.sharedInstance()

        do {
            try session.setCategory(.playAndRecord, mode: .spokenAudio,
                                    options: [.allowAirPlay, .allowBluetooth, .allowBluetoothA2DP,
                                              .defaultToSpeaker, .duckOthers])

            try session.setActive(true)
        }
        catch {
            Log.error(self, "Error while trying to start audio recording:", error)
        }

        if #available(iOS 17.0, *) {
            AVAudioApplication.requestRecordPermission(completionHandler: record)
        } else {
            session.requestRecordPermission(record)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        haptics = nil
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        uiDelegate?.criticalAlertDismissed()
    }


    // MARK: Actions

    @IBAction func prepareSend() {
        UIView.transition(with: sendingOverlay,
                          duration: 0.25,
                          options: .transitionCrossDissolve,
                          animations: { self.sendingOverlay.isHidden = false })
        { [weak self] _ in
            self?.send()
        }
    }

    @IBAction func cancel() {
        timeTimer?.invalidate()
        timeTimer = nil
        dbMeteringTimer?.invalidate()
        dbMeteringTimer = nil

        recorder?.stop()
        recorder = nil

        removeAudioFile()

        haptics?.notificationOccurred(.warning)

        dismiss(animated: true)
    }

    @IBAction func control(gesture: UIGestureRecognizer) {
        switch gesture.state {
        case .changed:
            if appeared {
                if sendBtCenterPoint == .zero {
                    sendBtCenterPoint = sendBt?.center ?? .zero
                }

                let loc = gesture.location(in: view)
                sendBtCenter.constant = sendBtCenterConstant + (sendBtCenterPoint.x - loc.x) * -1
                sendBtBottom.constant = sendBtBottomConstant + sendBtCenterPoint.y - loc.y
            }

        case .ended:
            if appeared, let cancelBt = cancelBt, let sendBt = sendBt {
                if cancelBt.frame.intersects(sendBt.frame) {
                    cancel()
                }
                else {
                    prepareSend()
                }
            }

        case .cancelled, .failed:
            if sendBtCenterPoint != .zero {
                sendBtCenter.constant = sendBtCenterConstant
                sendBtBottom.constant = sendBtBottomConstant
                sendBtCenterPoint = .zero
            }

        default:
            break
        }
    }


    // MARK: Private Methods

    private func send() {
        timeTimer?.invalidate()
        timeTimer = nil
        dbMeteringTimer?.invalidate()
        dbMeteringTimer = nil

        let time = recorder?.currentTime ?? 0
        recorder?.stop()
        recorder = nil

        var audioFile: URL?

        if time > 0.5, let url = Self.audioFile, (try? url.checkResourceIsReachable()) ?? false {
            audioFile = url
        }
        else {
            // If not used, delete again.
            removeAudioFile()
        }

        CleanInsightsManager.shared.measure(
            event: "status-created", name: "online",
            value: AFNetworkReachabilityManager.shared().isReachable ? 1 : 0)

        Task { [weak self] in
            var e: Error?

            do {
                try await Circles.shared.me()?.alert(audioFile: audioFile)

                if Settings.sendLocationWithAlerts {
                    // Make sure, location gets tracked until the alert is over.
                    LocationManager.shared.trackLocation(.year1)
                }
            }
            catch {
                e = error
            }

            var timeout: UInt64 = 1

            await MainActor.run {
                self?.activityIndicator.isHidden = true
                self?.sentImg.isHidden = false

                if let error = e {
                    self?.sentImg.image = .init(systemName: "exclamationmark.triangle")
                    self?.statusLb.text = "Error: %".localize(value: error.localizedDescription)
                    self?.haptics?.notificationOccurred(.error)

                    timeout = 3
                }
                else {
                    self?.statusLb.text = "Sent".localize()
                    self?.haptics?.notificationOccurred(.success)
                }
            }

            try? await Task.sleep(nanoseconds: timeout * 1_000_000_000)

            await MainActor.run {
                self?.dismiss(animated:true)
            }
        }
    }

    private func record(_ granted: Bool) {
        guard granted,
              let url = Self.audioFile
        else {
            return
        }

        removeAudioFile()

        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 16000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]

        recorder = try? AVAudioRecorder(url: url, settings: settings)
        recorder?.isMeteringEnabled = true
        recorder?.record()

        if recorder != nil {
            timeTimer = Timer.scheduledTimer(
                timeInterval: 1, target: self,
                selector: #selector(updateTime), userInfo: nil, repeats: true)

            dbMeteringTimer = Timer.scheduledTimer(
                timeInterval: 0.1, target: self,
                selector: #selector(updateDbMeter), userInfo: nil, repeats: true)
        }
    }

    @objc
    private func updateTime() {
        if let currentTime = recorder?.currentTime {
            durationLb.text = durationFormatter.string(from: currentTime)
        }
        else {
            durationLb.text = "00:00"
        }
    }

    @objc
    private func updateDbMeter() {
        dbMeterHeight.constant = dbMeterMaxHeight * (recorder?.scaledPower(forChannel: 0) ?? 0)
    }

    private func removeAudioFile() {
        if let audioFile = Self.audioFile {
            try? FileManager.default.removeItem(at: audioFile)
        }
    }
}
