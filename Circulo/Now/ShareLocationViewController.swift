//
//  ShareLocationViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 29.01.24.
//

import UIKit

protocol ShareLocationDelegate: AnyObject {

    func done(duration: LocationManager.Duration?)
}

class ShareLocationViewController: UIViewController {

    weak var delegate: ShareLocationDelegate?

    @IBOutlet weak var titleLb: UILabel! {
        didSet {
            titleLb.text = "How long?".localize()
        }
    }

    @IBOutlet weak var choice15MinBt: UIButton! {
        didSet {
            choice15MinBt.setTitle(LocationManager.Duration.minutes15.description)
        }
    }

    @IBOutlet weak var choice1HrBt: UIButton! {
        didSet {
            choice1HrBt.isSelected = true
            choice1HrBt.borderColor = .systemRed
            choice1HrBt.setTitle(LocationManager.Duration.minutes60.description)
        }
    }

    @IBOutlet weak var choice4HrBt: UIButton! {
        didSet {
            choice4HrBt.setTitle(LocationManager.Duration.minutes240.description)
        }
    }

    @IBOutlet weak var choice8hrBt: UIButton! {
        didSet {
            choice4HrBt.setTitle(LocationManager.Duration.minutes480.description)
        }
    }

    @IBOutlet weak var choiceArrivalBt: UIButton!
    @IBOutlet weak var arrivalContainer: UIView!
    @IBOutlet weak var arrivalDp: UIDatePicker! {
        didSet {
            arrivalDp.minimumDate = .init()
            arrivalDp.maximumDate = .init().addingTimeInterval(60 * 60 * 24 * 30)
            arrivalDp.minuteInterval = 5
        }
    }
    @IBOutlet weak var checkInLb: UILabel!
    @IBOutlet weak var checkInSw: UISwitch!
    @IBOutlet weak var shareBt: UIButton! {
        didSet {
            shareBt.setTitle("Share".localize())
            // Grrrr. This shouldn't be necessary, however, it is.
            shareBt.setTitleColor(.systemBackground)
        }
    }

    private var selectionBts = [UIButton?]()

    private var duration: LocationManager.Duration?


    init() {
        super.init(nibName: String(describing: Self.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        if let sheet = sheetPresentationController {
            sheet.detents = [.medium(), .large()]
            sheet.prefersGrabberVisible = true
            sheet.preferredCornerRadius = 16
        }

        selectionBts = [choice15MinBt, choice1HrBt, choice4HrBt, choice8hrBt, choiceArrivalBt]

        arrivalContainer.isHidden = true
        arrivalContainer.heightConstraint = 0
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        delegate?.done(duration: duration)
    }


    // MARK: Actions

    @IBAction func close() {
        dismiss(animated: true)
    }

    @IBAction func choose(_ sender: UIButton?) {
        for bt in selectionBts {
            bt?.borderColor = .systemGray
            bt?.isSelected = false
        }

        sender?.borderColor = .systemRed
        sender?.isSelected = true

        if sender == choiceArrivalBt {
            arrivalContainer.isHidden = false
            arrivalContainer.heightConstraint = nil
        }
        else {
            arrivalContainer.isHidden = true
            arrivalContainer.heightConstraint = 0
        }
    }

    @IBAction func share() {
        guard let selected = selectionBts.first(where: { $0?.isSelected ?? false }) else {
            return
        }

        switch selected {
        case choiceArrivalBt:
            duration = .until(arrival: arrivalDp.date)

        case choice15MinBt:
            duration = .minutes15

        case choice1HrBt:
            duration = .minutes60

        case choice4HrBt:
            duration = .minutes240

        case choice8hrBt:
            duration = .minutes480

        default:
            break
        }

        close()
    }
}
