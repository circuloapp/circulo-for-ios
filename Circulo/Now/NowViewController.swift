//
//  NowViewController.swift
//  Circulo
//
//  Created by N-Pex on 20.8.21.
//

import Keanu
import KeanuCore
import MatrixSDK
import CoreLocation
import AFNetworking

public protocol NowViewControllerDelegate: AnyObject {
    func refresh(done: (() -> Void)?)
}

public class NowViewController: UIViewController, CriticalAlertDelegate {

    // MARK: Storyboard outlets
    @IBOutlet weak var helloLb: UILabel!
    @IBOutlet weak var liveIndicatorIv: UIImageView!
    @IBOutlet weak var locationBt: UIButton! {
        didSet {
            // Annoying bug in Xcode or UIKit makes us do this.
            locationBt.imageView?.tintColor = .accent
        }
    }
    @IBOutlet weak var chatBt: UIButton! {
        didSet {
            // Annoying bug in Xcode or UIKit makes us do this.
            chatBt.imageView?.tintColor = .accent
        }
    }
    @IBOutlet weak var noActiveLb: UILabel!
    @IBOutlet weak var updatesContainer: UIView!
    @IBOutlet weak var alertBt: AlertButton!
    @IBOutlet weak var introductionSv: UIScrollView!
    @IBOutlet weak var updatesSv: UIScrollView!
    
    weak var delegate : NowViewControllerDelegate?

    private let refreshControl = UIRefreshControl()
    private var criticalAlertVc: CriticalAlertViewController?
    private var alertVc: AlertViewController?

    private var room: MXRoom? {
        Circles.shared.rooms.first
    }


    open override func viewDidLoad() {
        super.viewDidLoad()

        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        updatesSv.refreshControl = refreshControl
        updateViews()
    }

    @objc func refresh(_ sender: AnyObject) {
        if let delegate = delegate {
            delegate.refresh(done: {
                self.refreshControl.endRefreshing()
            })
        } 
        else {
            refreshControl.endRefreshing()
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: animated)

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(updateViews), name: .memberAdded, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .memberChanged, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .roomReady, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .locationTrackingEnded, object: nil)

        updateViews()
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        navigationController?.setNavigationBarHidden(false, animated: animated)

        NotificationCenter.default.removeObserver(self)
    }


    @objc
    func updateViews() {
        guard isViewLoaded else {
            return
        }

        introductionSv.isHidden = room != nil
        locationBt.isHidden = room == nil
        chatBt.isHidden = room == nil

        updatesContainer.subviews.forEach { $0.removeFromSuperview() }

        var lastStatusView: UIView? = nil

        if let room = room, !Circles.shared.isRoomReady(room) {
            let view = UIActivityIndicatorView(style: .large)

            view.translatesAutoresizingMaskIntoConstraints = false

            updatesContainer.addSubview(view)

            view.centerXAnchor.constraint(equalTo: updatesContainer.centerXAnchor).isActive = true
            view.topAnchor.constraint(equalTo: updatesContainer.topAnchor, constant: 8).isActive = true

            view.startAnimating()

            lastStatusView = view
        }

        liveIndicatorIv.isHidden = !LocationManager.shared.isTracking

        let view = MyLocationView.shared
        view.delegate = tabBarController as? MyLocationViewDelegate
        view.update()

        add(view: view, lastView: &lastStatusView)

        for member in Circles.shared.members(of: room) {
            guard !member.isMe else {
                continue
            }

            for issue in member.hasIssues {
                switch issue {
                case .alert:
                    let view = AlertView.create()
                    view.apply(member)

                    add(view: view, lastView: &lastStatusView)

                case .tracking:
                    let view = OtherLocationView.create()
                    view.apply(member)

                    add(view: view, lastView: &lastStatusView)
                }
            }
        }

        lastStatusView?.bottomAnchor.constraint(lessThanOrEqualTo: updatesContainer.bottomAnchor, constant: -8).isActive = true

        alertBt.isHidden = !(Circles.shared.sessionReady) || (Circles.shared.me(in: room)?.hasUnresolvedAlert ?? true)

        noActiveLb.isHidden = !updatesContainer.subviews.isEmpty
        helloLb.text = "Hello, %".localize(value: Circles.shared.account?.friendlyName ?? "")

        // If still starting up, hide the button "need help".
        if !Circles.shared.sessionReady {
            alertBt.isHidden = true
        }

        if Circles.shared.me(in: room)?.hasUnresolvedAlert ?? false {
            if alertVc == nil && criticalAlertVc == nil {
                let vc = AlertViewController()
                vc.member = Circles.shared.me(in: room)
                alertVc = vc

                present(vc, animated: true)
            }
        }
        else {
            alertVc?.dismiss(animated: true)
            alertVc = nil
        }
    }

    func selectMember(member: Member) {
        (tabBarController as? HomeViewController)?.selectMember(member: member)
    }


    // MARK: Actions

    @IBAction func shareLocation() {
        MyLocationView.shared.delegate = tabBarController as? MyLocationViewDelegate
        MyLocationView.shared.tap()
    }

    @IBAction func goToChat(_ sender: Any) {
        if let me = Circles.shared.me(in: room) {
            selectMember(member: me)

            let vc = CirculoRouter.shared.room()
            vc.room = room
            vc.allowStoryMode = false
            vc.showSettingsButton = false

            navigationController?.pushViewController(vc, animated: true)
        }
    }

    @IBAction func notifyNoAlert() {
        let pn = PopupNotification(frame: .zero)
        pn.labelText = "Tap hold to send alert.".localize()

        pn.attach(to: alertBt)

        pn.show()
    }

    @IBAction func createCriticalAlert(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            let vc = CriticalAlertViewController()
            criticalAlertVc = vc
            vc.modalPresentationStyle = .overFullScreen
            vc.uiDelegate = self

            present(vc, animated: true)
        }

        // Forward long press gesture to CriticalAlertViewController.
        criticalAlertVc?.control(gesture: gesture)
    }


    // MARK: CriticalAlertDelegate

    func criticalAlertDismissed() {
        criticalAlertVc = nil

        updateViews()
    }


    // MARK: Private Methods

    private func add(view: UIView, lastView: inout UIView?) {
        view.translatesAutoresizingMaskIntoConstraints = false

        updatesContainer.addSubview(view)

        view.leadingAnchor.constraint(equalTo: updatesContainer.leadingAnchor, constant: 16).isActive = true
        view.trailingAnchor.constraint(equalTo: updatesContainer.trailingAnchor, constant: -16).isActive = true

        let anchor = lastView?.bottomAnchor ?? updatesContainer.topAnchor
        view.topAnchor.constraint(equalTo: anchor, constant: 8).isActive = true

        lastView = view
    }
}
