//
//  CurrentLocationViewController.swift
//  Circulo
//
//  Created by Benjamin Erhart on 12.04.24.
//

import UIKit
import MapKit
import KeanuCore
import Contacts
import Keanu

class CurrentLocationViewController: UIViewController, UIGestureRecognizerDelegate {

    var member: Member?

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var pinBt: UIButton!
    @IBOutlet weak var avatar: AvatarView!
    @IBOutlet weak var locationLb: UILabel!
    @IBOutlet weak var showInMapsBt: UIButton! {
        didSet {
            showInMapsBt.setTitle("Show in Maps".localize())
        }
    }

    @IBOutlet weak var progressContainer: UIView!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var leftLb: UILabel!


    private var overlays = [MKOverlay]()

    private lazy var titleView: UIView = {
        let view = UIView()

        view.addSubview(nameLb)
        view.addSubview(lastUpdateLb)

        nameLb.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        nameLb.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        nameLb.topAnchor.constraint(equalTo: view.topAnchor).isActive = true

        lastUpdateLb.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        lastUpdateLb.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        lastUpdateLb.topAnchor.constraint(equalTo: nameLb.bottomAnchor).isActive = true
        lastUpdateLb.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        return view
    }()

    private lazy var nameLb: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .preferredFont(forTextStyle: .headline)

        return view
    }()

    private lazy var lastUpdateLb: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.font = .preferredFont(forTextStyle: .caption1)

        return view
    }()

    private lazy var workingOverlay: WorkingOverlay = {
        WorkingOverlay().addToSuperview(view)
    }()

    private var timer: Timer?

    private lazy var panGr: UIPanGestureRecognizer = {
        let gr = UIPanGestureRecognizer(target: self, action: #selector(mapDragged))
        gr.delegate = self

        return gr
    }()

    private var userInteractsWithMap = false


    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.titleView = titleView

        CirculoAnnotationView.member = member
        map.register(CirculoAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)

        avatar.load(member)

        if member?.isMe ?? false {
            nameLb.text = "My Location".localize()

            navigationItem.rightBarButtonItem = .init(barButtonSystemItem: .stop, target: self, action: #selector(stop))

            showInMapsBt.isHidden = true
        }
        else {
            nameLb.text = member?.displayName

            progressContainer.isHidden = true
        }

        update()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        map.addGestureRecognizer(panGr)

        timer = .scheduledTimer(timeInterval: 5, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        map.removeGestureRecognizer(panGr)

        timer?.invalidate()
        timer = nil
    }

    @objc
    func update() {
        if let track = member?.locationTrack {
            let locations = track.locations

            overlays = map.update(track: locations, old: overlays)

            if !userInteractsWithMap, let current = locations.last?.location {
                map.setRegion(current.location2d, size: 200)
            }

            locationLb.text = locations.last?.location?.locationDescription

            if let value = track.updated?.date.timeAgoDisplay() {
                lastUpdateLb.text = "Updated %".localize(value: value)
            }
            else {
                lastUpdateLb.text = nil
            }

            if member?.isMe ?? false {
                progressContainer.isHidden = false

                progress.progress = track.progress

                if let value = Formatters.format(friendlyDuration: track.left) {
                    leftLb.text = "% left".localize(value: value)
                    leftLb.isHidden = false
                }
                else {
                    leftLb.isHidden = true
                }
            }
            else {
                progressContainer.isHidden = true
            }
        }
        else {
            navigationController?.popViewController(animated: true)
        }
    }


    // MARK: Actions

    @IBAction func pin() {
        userInteractsWithMap = false

        if let current = member?.locationTrack?.locations.last?.location {
            map.setRegion(current.location2d, size: 200)
        }
    }

    @IBAction func stop(force: Bool = false) {
        guard navigationItem.rightBarButtonItem?.isEnabled ?? false
                && member?.isMe ?? false
        else {
            return
        }

        if !force && LocationManager.shared.isTracking {
            AlertHelper.present(
                self,
                message: "Are you sure you want to stop sharing your location with your circle?".localize(),
                title: "Stop Location Share".localize(),
                actions: [
                    AlertHelper.cancelAction(),
                    AlertHelper.destructiveAction("Stop".localize(), handler: { [weak self] _ in
                        self?.stop(force: true)
                    })])

            return
        }

        workingOverlay.isHidden = false

        LocationManager.shared.cancel(notify: false)

        Task { [weak self] in
            await self?.member?.redactMyLocations()

            _ = await MainActor.run {
                self?.navigationController?.popViewController(animated: true)
            }
        }
    }

    @IBAction func showInMaps() {
        guard let location = member?.getLastLocation()?.location else {
            return
        }

        let item = MKMapItem(placemark: location.placemark)
        item.name = member?.displayName
        item.openInMaps()
    }

    @objc private func mapDragged(_ gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state == .began {
            userInteractsWithMap = true
        }
    }


    // MARK: UIGestureRecognizerDelegate

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, 
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool
    {
        true
    }
}
