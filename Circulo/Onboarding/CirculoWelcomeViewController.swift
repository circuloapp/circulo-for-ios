//
//  CirculoWelcomeViewController.swift
//  Circulo
//
//  Created by N-Pex on 07.04.21.
//

import Keanu

class CirculoWelcomeViewController: WelcomeViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Go straight to add account view controller
        let vc = UIStoryboard(name: "Onboarding", bundle: nil).instantiateInitialViewController()
        navigationController?.viewControllers = [vc!]
    }
}
