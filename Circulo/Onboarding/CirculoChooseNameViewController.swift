//
//  CirculoChooseNameViewController.swift
//  Circulo
//
//  Created by N-Pex on 26.8.21.
//

import Keanu
import KeanuCore

public protocol CirculoChooseNameViewControllerDelegate: AnyObject {
    func createAccountWithName(name: String, avatar: UIImage?)
}

class CirculoChooseNameViewController: BaseViewController, UITextFieldDelegate, AvatarPickerViewControllerDelegate {

    weak var delegate : CirculoChooseNameViewControllerDelegate?

    @IBOutlet weak var avatarView: AvatarViewWithStatus!
    @IBOutlet weak var editName: UITextField!
    @IBOutlet weak var bottomLayoutConstraint: NSLayoutConstraint!
    
    private var pickedAvatarImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get random avatar
        let randomIndex = Int.random(in: 2..<18)
        avatarView.image = .avatar(randomIndex)
        
        editName.delegate = self
    }

    override open func keyboardWillShow(notification: Notification) {
        if let kbSize = getKeyboardSize(notification) {
            self.bottomLayoutConstraint.constant = kbSize.height
            animateDuringKeyboardMovement(notification)
        }
    }
    
    override open func keyboardWillBeHidden(notification: Notification) {
        bottomLayoutConstraint.constant = 0
        animateDuringKeyboardMovement(notification)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == editName, let name = editName?.text, name.count > 0 {
            // Create an account
            delegate?.createAccountWithName(name: name, avatar: pickedAvatarImage)
        }
        return true
    }
    
    @IBAction func didTapAvatar(_ sender: Any) {
        let avatarPicker = AvatarPickerViewController(collectionViewLayout: LeftAlignedCollectionViewFlowLayout())
        avatarPicker.delegate = self
        avatarPicker.modalPresentationStyle = .popover
        avatarPicker.popoverPresentationController?.sourceView = self.avatarView
        avatarPicker.popoverPresentationController?.sourceRect = self.avatarView.bounds
        self.present(avatarPicker, animated: true)
    }
    
    @IBAction func didTapCamera(_ sender: Any) {
        AvatarPicker.showPickerFor(avatarView: self.avatarView, inViewController: self) { image in
            self.pickedAvatarImage = image
        }
    }

    func didPickAvatar(_ avatarImage: UIImage) {
        if let image = MXKTools.resize(avatarImage, to: CGSize(width: 120, height: 120)) {
            self.pickedAvatarImage = image
            self.avatarView.image = image
        }
    }
    
}
