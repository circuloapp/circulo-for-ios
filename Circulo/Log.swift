//
//  Log.swift
//  Circulo
//
//  Created by Benjamin Erhart on 23.10.24.
//
import os.log

class Log {

    private static let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "main")


    class func debug(_ object: AnyObject, _ message: CustomStringConvertible) {
        logger.debug("[\(String(describing: type(of: object)))] \(message.description)")
    }

    class func error(_ object: AnyObject, _ message: CustomStringConvertible? = nil, _ error: Error? = nil) {
        if let message = message, !message.description.isEmpty {
            if let error = error {
                logger.error("[\(String(describing: type(of: object)))] \(message.description) \(error.localizedDescription)")
            }
            else {
                logger.error("[\(String(describing: type(of: object)))] \(message.description)")
            }
        }
        else if let error = error {
            logger.error("[\(String(describing: type(of: object)))] \(error.localizedDescription)")
        }
        else {
            logger.error("[\(String(describing: type(of: object)))] error")
        }
    }
}
