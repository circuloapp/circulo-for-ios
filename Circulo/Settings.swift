//
//  Settings.swift
//  Circulo
//
//  Created by Benjamin Erhart on 29.10.24.
//

import Foundation
import KeanuCore

class Settings {

    private static let kAllowAccessToMicrophone = "allow_access_to_microphone"
    private static let kSendLocationWithAlerts = "send_location_with_alerts"

    private class var defaults: UserDefaults? {
        MXKAppSettings.standard().sharedUserDefaults
    }


    /**
     Defaults to `true`!
     */
    class var allowAccessToMicrophone: Bool {
        get {
            if defaults?.object(forKey: kAllowAccessToMicrophone) == nil {
                return true
            }

            return defaults?.bool(forKey: kAllowAccessToMicrophone) ?? true
        }
        set {
            defaults?.set(newValue, forKey: kAllowAccessToMicrophone)
        }
    }

    /**
     Defaults to `true`!
     */
    class var sendLocationWithAlerts: Bool {
        get {
            if defaults?.object(forKey: kSendLocationWithAlerts) == nil {
                return true
            }

            return defaults?.bool(forKey: kSendLocationWithAlerts) ?? true
        }
        set {
            defaults?.set(newValue, forKey: kSendLocationWithAlerts)
        }
    }
}
