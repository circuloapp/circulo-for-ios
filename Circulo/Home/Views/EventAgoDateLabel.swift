//
//  AgoDateLabel.swift
//  Circulo
//
//  Created by N-Pex on 07.06.21.
//

import UIKit
import MatrixSDK

open class EventAgoDateLabel: UILabel {

    var updateTimer: Timer?

    private weak var event: MXEvent?

    deinit {
        stopUpdateTimer()
    }

    @discardableResult
    open func apply(event: MXEvent?) -> EventAgoDateLabel {
        self.event = event

        startMessageSeenTimer()
        update()

        return self
    }
    
    // Update the timestamp every minute
    func startMessageSeenTimer() {
        stopUpdateTimer()

        updateTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [weak self] timer in
            self?.update()
        }
    }
    
    func stopUpdateTimer() {
        updateTimer?.invalidate()
        updateTimer = nil
    }
    
    private func update() {
        if let event = event {
            text = event.originServerTs.date.timeAgoDisplay()
        }
        else {
            text = ""
        }
    }
}
