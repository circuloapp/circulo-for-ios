//
//  CircleAddView.swift
//  Circulo
//
//  Created by N-Pex on 18.8.21.
//

import UIKit

protocol CircleAddViewDelegate: AnyObject {

    func addMember()

    func selectMember(member: Member?)
}
    
open class CircleAddView: CirclePlacedView {

    weak var delegate: CircleAddViewDelegate?

    class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    class func createNew() -> CircleAddView {
        return CircleAddView.nib.instantiate(withOwner: nil, options: nil)[0] as! CircleAddView
    }
    
    open func apply() -> CircleAddView {
        return self
    }
        
    @IBAction func didTapView(sender:Any) {
        delegate?.addMember()
    }
}
