//
//  MemberCell.swift
//  Circulo
//
//  Created by N-Pex on 12.10.21.
//

import UIKit
import MatrixSDK
import KeanuCore

open class MemberCell: UITableViewCell {

    open class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }

    public class var defaultReuseId: String {
        return String(describing: self)
    }

    @IBOutlet weak var avatar: AvatarViewWithStatus!
    @IBOutlet weak var crown: UIView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var sublabel: UILabel!

    @discardableResult
    func apply(member: Member) -> MemberCell {
        avatar.member = member

        crown.isHidden = !(Circles.shared.room(of: member)?.isModerator(userId: member.mxMember.userId) ?? false)

        label.text = member.displayName

        if member.isMe {
            // String to append to the current user (name) in room members view.
            label.text?.append(" (You)".localize())
        }

        let friend = FriendsManager.shared.getOrCreate(member.mxMember, Circles.shared.session)
        sublabel.text = friend.friendlyPresence

        return self
    }
}
