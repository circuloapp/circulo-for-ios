//
//  AvatarViewWithStatus.swift
//  Circulo
//
//  Created by N-Pex on 29.04.21.
//

import KeanuCore
import Keanu
import MatrixSDK
import PureLayout


class AvatarViewWithStatus: AvatarView {
    
    public var member: Member? {
        didSet {
            if let member = member {
                if let session = Circles.shared.session {
                    load(session: session, id: member.id,
                         avatarUrl: member.mxMember.avatarUrl, displayName: member.displayName)
                }
                else {
                    load(friend: friend)
                }
            }
            else {
                image = nil
            }

            setNeedsLayout()
        }
    }

    private var friend: Friend? {
        guard let member = member else {
            return nil
        }

        return FriendsManager.shared.getOrCreate(member.mxMember)
    }

    public override func layoutSubviews() {
        super.layoutSubviews()

        guard let member = member else {
            return
        }

        if !member.haveRecognizedAlert || member.isMe {
            if member.hasUnresolvedAlert {
                setRedBorder()
            }
            else {
                setGradientBorder()
            }
        }
        else {
            setGrayBorder()
        }
    }
    
    private func setGradientBorder() {
        layer.borderColor = UIColor.clear.cgColor
        setGradientBorder(width: borderWidth, colors: [UIColor(hexString: "#ffb03fa4"), UIColor(hexString: "#ff704aa6")])
    }
    
    private func setRedBorder() {
        layer.borderColor = UIColor.clear.cgColor
        setGradientBorder(width: borderWidth, colors: [UIColor(hexString: "#ffff4141"), UIColor(hexString: "#ffff4141")])
    }
    
    private func setGrayBorder() {
        layer.borderColor = UIColor.clear.cgColor
        setGradientBorder(width: borderWidth, colors: [UIColor(hexString: "#ffd1d1d1"), UIColor(hexString: "#ffd1d1d1")])
    }
}

public extension UIView {

    private static let kLayerNameGradientBorder = "GradientBorderLayer"

    func setGradientBorder(
        width: CGFloat,
        colors: [UIColor],
        startPoint: CGPoint = CGPoint(x: 0, y: 0),
        endPoint: CGPoint = CGPoint(x: 1, y: 0)
    ) {
        let existedBorder = gradientBorderLayer()
        let border = existedBorder ?? CAGradientLayer()
        border.frame = bounds
        border.colors = colors.map { return $0.cgColor }
        border.startPoint = startPoint
        border.endPoint = endPoint

        let mask = CAShapeLayer()
        mask.path = UIBezierPath(roundedRect: bounds, cornerRadius: frame.size.height / 2).cgPath
        mask.fillColor = UIColor.clear.cgColor
        mask.strokeColor = UIColor.white.cgColor
        mask.lineWidth = width

        border.mask = mask

        let exists = existedBorder != nil
        if !exists {
            layer.addSublayer(border)
        }
    }


    fileprivate func gradientBorderLayer() -> CAGradientLayer? {
        let borderLayers = layer.sublayers?.filter { return $0.name == UIView.kLayerNameGradientBorder }
        if borderLayers?.count ?? 0 > 1 {
            fatalError()
        }
        return borderLayers?.first as? CAGradientLayer
    }
}
