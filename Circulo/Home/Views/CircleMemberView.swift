//
//  CircleMemberView.swift
//  Circulo
//
//  Created by N-Pex on 18.8.21.
//

import UIKit
import KeanuCore

protocol CircleMemberViewDelegate: AnyObject {

    func selectMember(member: Member?)
}

open class CircleMemberView: CirclePlacedView {
    
    @IBOutlet weak var avatarView: AvatarViewWithStatus!
    @IBOutlet weak var label: UILabel!
    
    weak var delegate: CircleMemberViewDelegate?
    
    class var nib: UINib {
        return UINib(nibName: String(describing: self), bundle: Bundle(for: self))
    }
    
    class func createNew() -> CircleMemberView {
        let view = CircleMemberView.nib.instantiate(withOwner: nil, options: nil)[0] as! CircleMemberView
        view.avatarView.rounded = false
        view.avatarView.layer.cornerRadius = 0
        view.avatarView.clipsToBounds = true
        view.avatarView.layer.masksToBounds = true
        return view
    }
    
    @discardableResult
    func apply(member: Member?) -> CircleMemberView {
        guard let member = member else {
            return self
        }

        avatarView.member = member
        label.text = member.displayName
        alpha = member.mxMember.membership == .invite ? 0.4 : 1

        return self
    }
        
    @IBAction func didTapView(sender:Any) {
        delegate?.selectMember(member: avatarView.member)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        avatarView.layer.cornerRadius = avatarView.layer.bounds.width / 4
    }
}
