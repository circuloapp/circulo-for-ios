//
//  OurCircleViewController.swift
//  Circulo
//
//  Created by N-Pex on 18.8.21.
//

import Foundation

import Keanu
import KeanuCore
import MatrixSDK
import CoreLocation
import MaterialComponents.MaterialSnackbar

public class OurCircleViewController: UIViewController, CircleAddViewDelegate & CircleMemberViewDelegate {

    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    // MARK: Storyboard outlets
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var circleNameLabel: UILabel!
    @IBOutlet weak var circleContainerView: UIView!
    @IBOutlet weak var lockCircleView: UIView!
    @IBOutlet weak var scrollViewIntroduction: UIScrollView!
    

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        updateViews()

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(updateViews), name: .memberAdded, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .memberChanged, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .roomReady, object: nil)
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }


    public func addMember() {
        guard let room = Circles.shared.rooms.first else {
            return
        }

        room.state({ state in
            if !(state?.isJoinRulePublic ?? true) {
                let actions = [
                    AlertHelper.cancelAction(),
                    AlertHelper.defaultAction("Yes".localize()) { action in
                        self.setCircleLocked(locked: false) { success in
                            if success {
                                (self.tabBarController as? HomeViewController)?.addMember()
                            }
                        }
                    },
                ]
                AlertHelper.present(
                    self, message: nil,
                    title: "Open invite link to add people?".localize(),
                    actions: actions)
            } else {
                (self.tabBarController as? HomeViewController)?.addMember()
            }
        })
    }
    
    func selectMember(member: Member?) {
        (tabBarController as? HomeViewController)?.selectMember(member: member)
    }

    public func onRoomCreated(room: MXRoom) {
        //circleStackDataSource?.selectCircleByRoomId(roomId: room.roomId)
    }
    
    @objc func updateViews() {
        guard isViewLoaded else {
            return
        }

        let room = Circles.shared.rooms.first

        scrollViewIntroduction.isHidden = (room != nil)
        buttonEdit.isHidden = (room == nil)
        circleNameLabel.text = room?.summary?.displayName ?? ""

        circleContainerView.subviews.forEach({ v in
            if v is CirclePlacedView {
                v.removeFromSuperview()
            }
        })

        let members = Circles.shared.members(of: room).sorted()

        for i in 0...5 {
            let view: CirclePlacedView

            if i < members.count && members[i].leave == nil {
                let mv = CircleMemberView.createNew()
                mv.delegate = self
                view = mv.apply(member: members[i])
            }
            else {
                let av = CircleAddView.createNew()
                av.delegate = self
                view = av
            }

            view.angle = 270 - Double(i) * 60
            circleContainerView.addSubview(view)
        }

        // Show or hide lock circle view
        if let room = room, room.canModifyJoinRule() {
            room.state({ state in
                self.lockCircleView.isHidden = !(state?.isJoinRulePublic ?? true)
                self.lockCircleView.alpha = 1
            })
        } 
        else {
            self.lockCircleView.isHidden = true
        }
    }


    @IBAction func didTapLockCircle(sender:Any) {
        setCircleLocked(locked: true) { success in
            // Show toast with undo button
            if success {
                let action = MDCSnackbarMessageAction()
                action.title = "Undo".localize()
                action.handler = {
                    self.setCircleLocked(locked: false)
                }
                let message = MDCSnackbarMessage()
                message.duration = 5
                message.action = action
                message.text = "Invite link disabled".localize()
                MDCSnackbarManager.default.show(message)
            }
        }
    }
    
    func setCircleLocked(locked: Bool, done:((Bool) -> Void)? = nil) {
        guard let room = Circles.shared.rooms.first else {
            done?(false)
            return
        }

        workingOverlay.isHidden = false

        room.setJoinRule(locked ? .invite : .public) { [weak self] response in
            self?.workingOverlay.isHidden = true

            if response.isSuccess {
                UIView.animate(withDuration: 0.3) {
                    self?.lockCircleView.alpha = locked ? 0 : 1
                } completion: { success in
                    self?.lockCircleView.isHidden = locked
                }
            }

            done?(response.isSuccess)
        }
    }
}
