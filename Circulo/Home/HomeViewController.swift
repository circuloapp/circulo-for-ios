//
//  HomeViewController.swift
//  Circulo
//
//  Created by N-Pex on 26.04.21.
//

import Keanu
import KeanuCore
import MatrixSDK
import UIKit
import MaterialComponents.MaterialSnackbar
import AFNetworking
import PureLayout

public class HomeViewController: UITabBarController, ChatListProtocol, NowViewControllerDelegate,
    CirculoProfileViewControllerDelegate, JoinCircleViewControllerDelegate,
    CreateCircleViewControllerDelegate, UITabBarControllerDelegate, MyLocationViewDelegate
{
    
    private var membersDataSourceIsTemporary: Bool = true // We set a temporary DS when loading, to not show a blank screen

    private var pendingInvite: (id: String, circleName: String?)?


    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()

    open override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        refresh()
    }
        
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // Tab indicator as a line
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: .accent, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height:  tabBar.frame.height), lineWidth: 2.0)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.setNavigationBarHidden(true, animated: animated)

        let nc = NotificationCenter.default

        nc.addObserver(self, selector: #selector(reachabilityChanged),
                       name: .AFNetworkingReachabilityDidChange, object: nil)

        nc.addObserver(self, selector: #selector(updateCircle), name: .roomReady, object: nil)

        nc.addObserver(self, selector: #selector(sessionStateDidChange), name: .mxSessionStateDidChange, object: nil)

        for viewController in self.viewControllers ?? [] {
            if let vc = (viewController as? UINavigationController)?.viewControllers.first as? NowViewController {
                vc.delegate = self
            }
            else if let vc = viewController as? CirculoProfileViewController {
                vc.delegate = self
            }
        }

        delegate = self
    }

    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        delegate = nil

        NotificationCenter.default.removeObserver(self)

        navigationController?.setNavigationBarHidden(false, animated: animated)
    }


    // MARK: Invite Link Handling

    @objc
    private func sessionStateDidChange(_ notification: Notification) {
        guard Circles.shared.sessionReady,
              let pendingInvite = pendingInvite
        else {
            return
        }

        didReceiveCircleInvite(id: pendingInvite.id, circleName: pendingInvite.circleName)
    }

    public func didReceiveCircleInvite(id: String, circleName: String?) {
        if !Circles.shared.sessionReady {
            pendingInvite = (id: id, circleName: circleName)
        } 
        else {
            pendingInvite = nil

            if let room = Circles.shared.rooms.first {
                guard id != room.roomId else {
                    return
                }

                // Already in a circle, ask to replace
                let message = (circleName != nil) 
                    ? "You've been invited to the ´%´ circle. Would you like to leave your current circle to join it?".localize(values: circleName!)
                    : "You've been invited to a new circle. Would you like to leave your current circle to join it?".localize()

                let title = (circleName != nil)
                    ? "Leave ´%´ to join ´%´?".localize(values: room.summary?.displayName ?? "", circleName!)
                    : "Leave ´%´ to join new circle".localize(values: room.summary?.displayName ?? "")

                let alert = AlertHelper.build(message: message, title: title, style: .alert, actions: nil)
                alert.addAction(AlertHelper.defaultAction("Not now".localize()))
                alert.addAction(AlertHelper.defaultAction(
                    "Yes".localize(),
                    handler: { [weak self] _ in
                        self?.replaceCircle(currentCircle: room.roomId, newCircle: id)
                    }))

                if let ppc = alert.popoverPresentationController {
                    ppc.sourceView = self.view
                    ppc.sourceRect = self.view.bounds
                }
                present(alert, animated: true, completion: nil)
            } 
            else {
                // Not in a circle, ask to join
                let message = (circleName != nil) 
                    ? "You've been invited to the ´%´ circle.".localize(values: circleName!)
                    : "You've been invited to a circle.".localize()

                let alert = AlertHelper.build(message: message, title: "Join a Circle".localize(), style: .alert, actions: nil)
                alert.addAction(AlertHelper.defaultAction("Not now".localize()))
                alert.addAction(AlertHelper.defaultAction(
                    "Join".localize(),
                    handler: { [weak self] action in
                        self?.joinRoom(id: id)
                    }))

                if let ppc = alert.popoverPresentationController {
                    ppc.sourceView = self.view
                    ppc.sourceRect = self.view.bounds
                }
                present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func maybeShowBadgeOnOurCircle() {
        guard let ourCircleItem = tabBar.items?[1] else {
            return
        }

        if Circles.shared.sessionReady && Circles.shared.rooms.isEmpty {
            // No rooms, show badge on "our circle" to prompt user to create or join!
            ourCircleItem.setBadgeTextAttributes([.font : UIFont.systemFont(ofSize: 8), .foregroundColor: UIColor.systemRed], for: .normal)
            ourCircleItem.badgeColor = .clear
            ourCircleItem.badgeValue = "●"
        }
        else {
            ourCircleItem.badgeValue = nil
        }
    }

    /**
     Override "present" to instead "push" Room view controllers.
     */
    public override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        if let vc = viewControllerToPresent as? UINavigationController,
           let firstChild = vc.viewControllers.first,
           firstChild is RoomViewController
        {
            navigationController?.pushViewController(firstChild, animated: true)
            completion?()
            return
        }
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    
    public func openRoom(_ room: MXRoom?) {
    }
    
    public func createEmptyRoom(session: MXSession) {
    }
    
    public func friendsChosen(_ friends: [Friend]) {
    }


    // MARK: CreateCircleViewControllerDelegate

    public func onRoomCreated(room: MXRoom) {
        updateCircle()
    }

    /**
     Update the current circle. Since we have only one, we grab the first room if any.
     */
    @objc
    func updateCircle(_ notification: Notification? = nil) {
        guard let session = Circles.shared.session else {
            maybeShowBadgeOnOurCircle()
            return
        }
        
        let firstJoinedRoom = session.rooms.first { room in
            session.isJoined(onRoom: room.roomId)
        }

        if let firstJoinedRoom = firstJoinedRoom {
            if Circles.shared.sessionReady {
                guard membersDataSourceIsTemporary || (Circles.shared.rooms.first?.roomId != firstJoinedRoom.roomId) else {
                    return // no change
                }

                membersDataSourceIsTemporary = false
            }
            else {
                // Not fully initialized, but we definitely have at least one circle!
                // Show loading indicator...
                guard Circles.shared.rooms.first?.roomId != firstJoinedRoom.roomId else {
                    return // no change
                }

                membersDataSourceIsTemporary = true
            }
        } 

        maybeShowBadgeOnOurCircle()
    }
    
    func selectMember(member: Member?) {
        guard let member = member else {
            return
        }

        if member == Circles.shared.me() {
            viewOrEditStatus()
        } 
        else if member.hasUnresolvedAlert {
            self.performSegue(withIdentifier: "showMessage", sender: member)
        }
    }
    
    public func addMember() {
//        let vc = CirculoRouter.shared.addFriend()
//        vc.startChatWithAddedFriend = false
//        vc.friendAddedCallback = { friend in
//            // Added a friend, invite to this circle!
//            if let room = self.membersDataSource?.room {
//                room.invite(.userId(friend)) { response in
//                    // TODO
//                }
//            }
//        }
//        navigationController?.pushViewController(vc, animated: true)
        guard
            let id = Circles.shared.rooms.first?.roomId,
            let shareUrl = URL(string: String(format: Config.inviteLinkFormat, id)) else {
                return
            }
    
        let activityVc = UIActivityViewController(activityItems: [InviteLinkItem(url: shareUrl)], applicationActivities: [ShowQrActivity()])
        activityVc.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.saveToCameraRoll, UIActivity.ActivityType.addToReadingList]
        activityVc.popoverPresentationController?.sourceView = self.view
        activityVc.popoverPresentationController?.sourceRect = self.view.bounds
        self.present(activityVc, animated: true)
    }
    
    public func refresh(done: (() -> Void)? = nil) {

        workingOverlay.isHidden = false

        Task {
            var hasError = false

            do {
                try await Circles.shared.reload()
            }
            catch {
                hasError = true
            }

            await MainActor.run {
                updateCircle()

                // Update view. Display name etc. has changed.
                for viewController in viewControllers ?? [] {
                    if let vc = (viewController as? UINavigationController)?.viewControllers.first as? NowViewController {
                        vc.updateViews()
                    }
                    else if let vc = viewController as? OurCircleViewController {
                        vc.updateViews()
                    }
                }

                if let pendingInvite = pendingInvite {
                    didReceiveCircleInvite(id: pendingInvite.id, circleName: pendingInvite.circleName)
                }

                workingOverlay.isHidden = true
                done?()

                if hasError {
                    let actions = [
                        AlertHelper.defaultAction("Stop App".localize(), handler: { _ in
                            UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                exit(0)
                            }
                        })]

                    AlertHelper.present(
                        self,
                        message: "There was an error connecting to the server.".localize() + "\n\n"
                        + "Stop the app and open it again.".localize(),
                        actions: actions)
                }
            }
        }
    }
    
    @IBAction public func viewOrEditStatus() {
        guard let member = Circles.shared.me() else {
            return
        }

        // TODO: Remove all of this, this doesn't work anymore.

        if member.hasUnresolvedAlert {
            // We have an active update, go to view
            self.performSegue(withIdentifier: "showMessage", sender: member)
        }
    }

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CreateCircleViewController {
            vc.delegate = self
        } else if let vc = segue.destination as? JoinCircleViewController {
            vc.delegate = self
        }
    }
    
    private func joinRoom(id: String) {
        guard let session = Circles.shared.session else {
            return
        }

        workingOverlay.isHidden = false

        Task {
            do {
                let room = try await Circles.shared.join(roomIdOrAlias: id)

                await MainActor.run {
                    workingOverlay.isHidden = true
                    joinedRoom(id: room.roomId)
                }
            }
            catch {
                await MainActor.run {
                    workingOverlay.isHidden = true

                    let message = MDCSnackbarMessage()
                    message.duration = 3
                    message.text = error.localizedDescription
                    MDCSnackbarManager.default.show(message)
                }
            }
        }

        session.joinRoom(id, viaServers: nil, withSignUrl: nil) { [weak self] response in
            self?.workingOverlay.isHidden = true

            if response.isSuccess {
                self?.joinedRoom(id: id)
            }
            else if let error = response.error {
                let message = MDCSnackbarMessage()
                message.duration = 3
                message.text = error.localizedDescription
                MDCSnackbarManager.default.show(message)
            }
        }
    }
    
    private func replaceCircle(currentCircle: String, newCircle: String) {
        guard let session = Circles.shared.session else {
            return
        }

        workingOverlay.isHidden = false

        session.leaveRoom(currentCircle) { [weak self] response in
            if response.isSuccess {
                self?.joinRoom(id: newCircle)
            }
            else if let error = response.error {
                let message = MDCSnackbarMessage()
                message.duration = 3
                message.text = error.localizedDescription
                MDCSnackbarManager.default.show(message)
            }
        }
    }
    
    public func joinedRoom(id: String) {
        navigationController?.popToViewController(self, animated: true)

        refresh()
    }
    
    @IBAction func didTapCreateCircle(_ sender: Any) {
        performSegue(withIdentifier: "segueToCreateCircle", sender: self)
    }

    @IBAction func didTapJoinCircle(_ sender: Any) {
        performSegue(withIdentifier: "segueToJoinCircle", sender: self)
    }


    // MARK: Reachability Handling

    private lazy var noInternetView: UIView = {
        let view = UIView(forAutoLayout: ())
        view.backgroundColor = .accent
        view.autoSetDimension(.height, toSize: 20)

        let icon = UIImageView(forAutoLayout: ())
        icon.contentMode = .scaleAspectFit
        icon.image = .noNetwork?.imageMaskedAndTinted(with: .white)

        let label = UILabel(forAutoLayout: ())
        label.textColor = .white
        label.text = "No Internet".localize()

        let container = UIView(forAutoLayout: ())
        container.addSubview(icon)
        container.addSubview(label)

        icon.autoPinEdge(toSuperviewEdge: .leading)
        icon.autoPinEdge(toSuperviewEdge: .top, withInset: 2)
        icon.autoPinEdge(toSuperviewEdge: .bottom, withInset: 2)

        label.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .leading)
        label.autoPinEdge(.leading, to: .trailing, of: icon, withOffset: 8)

        view.addSubview(container)
        container.autoPinEdge(toSuperviewEdge: .top)
        container.autoPinEdge(toSuperviewEdge: .bottom)
        container.autoAlignAxis(toSuperviewAxis: .vertical)

        return view
    }()

    @objc
    private func reachabilityChanged(_ notification: Notification) {
        let manager = notification.object as? AFNetworkReachabilityManager

        if !(manager?.isReachable ?? true) {
            if let parent = selectedViewController?.view,
               noInternetView.superview != parent
            {
                parent.addSubview(noInternetView)

                noInternetView.autoPinEdge(toSuperviewEdge: .leading)
                noInternetView.autoPinEdge(toSuperviewEdge: .trailing)
                noInternetView.autoPinEdge(toSuperviewSafeArea: .top)
            }
        }
        else {
            noInternetView.removeFromSuperview()
        }
    }


    // MARK: UITabBarControllerDelegate

    public func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let n = Notification(name: .AFNetworkingReachabilityDidChange,
                             object: AFNetworkReachabilityManager.shared())

        reachabilityChanged(n)
    }
}

fileprivate extension String {
    func maybeSpace() -> String {
        if self.count > 0 {
            return " "
        }
        return ""
    }
}

class InviteLinkItem: NSObject, UIActivityItemSource {
    var url:URL
    init(url: URL) {
        self.url = url
        super.init()
    }
    
    public func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return url
    }

    public func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        return url
    }

    public func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "Join my circle".localize()
    }
}
