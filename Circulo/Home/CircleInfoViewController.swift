//
//  CircleInfoViewController.swift
//  Circulo
//
//  Created by N-Pex on 08.10.21.
//

import Keanu
import KeanuCore
import MatrixSDK
import CoreLocation

public protocol CircleInfoViewControllerDelegate: AnyObject {
}

public class CircleInfoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CircleEditDetailsViewControllerDelegate {

    //@IBOutlet weak var avatarView: AvatarViewWithStatus!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTopic: UILabel!
    @IBOutlet weak var labelCreatedBy: UILabel!
    @IBOutlet weak var tableView: UITableView!

    weak var delegate : CircleInfoViewControllerDelegate?
    private var sessionStateObserver: NSObjectProtocol?
    private var roomSummaryObserver: NSObjectProtocol?
    
    @MainActor
    private lazy var workingOverlay: WorkingOverlay = {
        return WorkingOverlay().addToSuperview(view)
    }()
    
    private enum TableSection {
        case editDetails
        case joinStatus
        case members
        case leave
        case admin
    }

    private enum TableRow {
        case invalid
        case editDetails
        case joinStatusOpen
        case joinStatusClosed
        case shareLink
        case showQr
        case leave
        case delete
    }

    private var data:[(TableSection,[TableRow])] = []

    private var room: MXRoom? {
        Circles.shared.rooms.first
    }

    private var members: [Member] {
        Circles.shared.members(of: room)
    }

    
    // MARK: Properties

    open override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ImageButtonCell.nib, forCellReuseIdentifier: ImageButtonCell.defaultReuseId)
        tableView.register(MemberCell.nib, forCellReuseIdentifier: MemberCell.defaultReuseId)
        tableView.layoutMargins = .zero
        tableView.contentInset = .zero
        tableView.separatorInset = .zero
        updateData()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateViews()

        let nc = NotificationCenter.default

        sessionStateObserver = nc.addObserver(
            forName: .mxSessionStateDidChange, object: nil,
            queue: .main, using: { _ in self.updateViews() })

        roomSummaryObserver = nc.addObserver(
            forName: .mxRoomSummaryDidChange, object: nil,
            queue: .main, using: { notification in
                if notification.object as? MXRoomSummary == self.room?.summary {
                    self.updateViews()
                }
            })

        nc.addObserver(self, selector: #selector(updateViews), name: .roomReady, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .memberAdded, object: nil)
        nc.addObserver(self, selector: #selector(updateViews), name: .memberChanged, object: nil)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        let nc = NotificationCenter.default

        nc.removeObserver(self)

        if let sessionStateObserver = sessionStateObserver {
            nc.removeObserver(sessionStateObserver)
            self.sessionStateObserver = nil
        }

        if let roomSummaryObserver = roomSummaryObserver {
            nc.removeObserver(roomSummaryObserver)
            self.roomSummaryObserver = nil
        }
    }


    @objc func updateViews() {
        guard isViewLoaded,
              let room = room
        else {
            return
        }

        labelName.text = room.summary.displayName
        labelTopic.text = room.summary.topic
        labelTopic.isHidden = (room.summary.topic?.count ?? 0) > 0 ? false : true

        if let creator = room.summary.creatorUserId,
           let creatingMember = Circles.shared.member(of: room, with: creator)
        {
            labelCreatedBy.text = String(format: "Created by %".localize(value: creatingMember.displayName))
            labelCreatedBy.isHidden = false
        } 
        else {
            labelCreatedBy.isHidden = true
        }

        updateData()
    }
    
    func updateData() {
        data = []

        if let room = room, room.canModifyName(), room.canModifyTopic() {
            data.append((.editDetails, [.editDetails]))
        }

        if let room = room, let state = room.dangerousSyncState, state.isJoinRulePublic {
            data.append((.joinStatus, [.joinStatusOpen, .shareLink, .showQr]))
        } 
        else {
            data.append((.joinStatus, [.joinStatusClosed]))
        }

        data.append((.members, []))
        data.append((.leave, [.leave]))

        if let room = room, room.isAdmin(userId: Circles.shared.myUserId) {
            data.append((.admin, [.delete]))
        }

        self.tableView.reloadData()
    }
    
   private  func dataForIndexPath(indexPath: IndexPath) -> (TableSection, TableRow) {
        let (sectionType,dataRows) = data[indexPath.section]
        return (sectionType, dataRows.count > indexPath.row ? dataRows[indexPath.row] : .invalid)
    }


    public func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let (sectionType,dataRows) = data[section]

        if [.editDetails, .joinStatus, .leave, .admin].contains(sectionType) {
            return dataRows.count
        }
        else if sectionType == .members {
            return members.count
        }

        return 0
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let (sectionType, _) = data[section]
        let sectionTypeHeaders:[TableSection:String] = [
            .editDetails: " ",
            .joinStatus: "Join Status".localize(),
            .members: "Members".localize(),
            .leave: " ",
            .admin: "Admin only".localize()
        ]
        return sectionTypeHeaders[sectionType]?.uppercased()
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.tintColor = .white
            headerView.textLabel?.textColor = .accent
            headerView.layoutMargins = .zero
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let (sectionType, dataRows) = data[indexPath.section]

        if sectionType == .members {
            let cell = tableView.dequeueReusableCell(withIdentifier: MemberCell.defaultReuseId, for: indexPath) as! MemberCell

            let members = self.members

            // Avoid race condition crash.
            if indexPath.row < members.count {
                cell.apply(member: members[indexPath.row])
            }

            return cell
        }

        switch dataRows[indexPath.row] {
        case .editDetails:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageButtonCell.defaultReuseId, for: indexPath) as! ImageButtonCell
            cell.apply(.editDetails, "Edit Details".localize())

            if let room = room, room.canModifyName(), room.canModifyTopic() {
                cell.selectionStyle = .default
            } 
            else {
                cell.selectionStyle = .none
            }

            return cell

        case .joinStatusOpen:
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "twoline")
            cell.textLabel?.text = "Open".localize()
            cell.detailTextLabel?.text = "Anyone with a link can join".localize()
            cell.accessoryType = .disclosureIndicator

            return cell

        case .joinStatusClosed:
            let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "twoline")
            cell.textLabel?.text = "Closed".localize()
            cell.detailTextLabel?.text = "Only people added are in your circle".localize()
            cell.accessoryType = .disclosureIndicator

            return cell

        case .shareLink:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageButtonCell.defaultReuseId, for: indexPath) as! ImageButtonCell
            cell.apply(.shareLink, "Share Link".localize())
            cell.iconColor = .accent
            cell.labelColor = .accent

            return cell

        case .showQr:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageButtonCell.defaultReuseId, for: indexPath) as! ImageButtonCell
            cell.apply(.qr, "Show QR".localize())
            cell.iconColor = .accent
            cell.labelColor = .accent

            return cell

        case .leave:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageButtonCell.defaultReuseId, for: indexPath) as! ImageButtonCell
            cell.apply(.leave, "Leave".localize())
            cell.iconColor = .systemRed
            cell.labelColor = .systemRed

            return cell

        case .delete:
            let cell = tableView.dequeueReusableCell(withIdentifier: ImageButtonCell.defaultReuseId, for: indexPath) as! ImageButtonCell
            cell.apply(.delete, "Delete Circle".localize())
            cell.iconColor = .systemRed
            cell.labelColor = .systemRed

            return cell

        case .invalid:
            break
        }

        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let (sectionType,rowType) = dataForIndexPath(indexPath: indexPath)

        if sectionType == .members {
            didSelect(member: members[indexPath.row].mxMember, at: indexPath)
            return
        }

        switch rowType {
        case .editDetails:
            self.performSegue(withIdentifier: "editDetails", sender: self)
            break

        case .joinStatusOpen, .joinStatusClosed:
            // DO we have power to change?
            guard let room = room, room.canModifyJoinRule() else {
                return
            }

            AlertHelper.present(
                self, message:
                    "Open - Anyone with a link can join\nClosed - Only people added are in your circle".localize(),
                title: "Join Status".localize(),
                actions: [
                    AlertHelper.cancelAction(),
                    AlertHelper.defaultAction("Open".localize()) { [weak self] action in
                        self?.changeJoinStatus(open: true)
                    },
                    AlertHelper.defaultAction("Closed".localize()) { [weak self] action in
                        self?.changeJoinStatus(open: false)
                    }
                ])
            break

        case .shareLink:
            guard let id = room?.roomId,
                  let shareUrl = URL(string: String(format: Config.inviteLinkFormat, id))
            else {
                return
            }
        
            let activityVc = UIActivityViewController(activityItems: [InviteLinkItem(url: shareUrl)], applicationActivities: nil)
            activityVc.excludedActivityTypes = [UIActivity.ActivityType.print, UIActivity.ActivityType.saveToCameraRoll, UIActivity.ActivityType.addToReadingList]
            activityVc.popoverPresentationController?.sourceView = self.view
            activityVc.popoverPresentationController?.sourceRect = self.view.bounds
            present(activityVc, animated: true)
            break

        case .showQr:
            guard let id = room?.roomId,
                  let shareUrl = URL(string: String(format: Config.inviteLinkFormat, id))
            else {
                return
            }

            let vc = UIApplication.shared.router.showQr()
            vc.qrCode = shareUrl.absoluteString
            let navVc = UINavigationController(rootViewController: vc)
            navVc.popoverPresentationController?.sourceView = self.view
            navVc.popoverPresentationController?.sourceRect = self.view.bounds
            present(navVc, animated: true, completion: nil)
            break

        case .leave:
            AlertHelper.present(
                self,
                message: "Are you sure you want to leave the current circle?".localize(),
                title: "Leave circle?".localize(),
                actions: [
                    AlertHelper.defaultAction("Leave".localize()) { [weak self] action in
                        self?.leaveCircle()
                    },
                    AlertHelper.cancelAction()
                ])
            break

        case .delete:
            AlertHelper.present(
                self,
                message: "Are you sure you want to delete the current circle?".localize(),
                title: "Delete circle?".localize(),
                actions: [
                    AlertHelper.destructiveAction("Delete".localize()) { [weak self] action in
                        self?.deleteCircle()
                    },
                    AlertHelper.cancelAction()
                ])
            break

        default: 
            break
        }
    }
    
    func didSelect(member: MXRoomMember, at indexPath: IndexPath) {
        guard let room = room else {
            return
        }

        if let me = Circles.shared.me(in: room), me.mxMember == member {
            return // Tapped ourselves, no-op
        }
        
        let alert = AlertHelper.build(title: nil, style: .actionSheet, actions: nil)
        if room.canMakeUserAdmin(userId: member.userId) {
            // Action to make someone an admin for a room.
            alert.addAction(AlertHelper.defaultAction("Make admin".localize())
            { [weak self] action in
                self?.makeAdmin(member: member)
            })
        }
        
        if room.canMakeUserModerator(userId: member.userId) {
            // Action to make someone a moderator for a room.
            alert.addAction(AlertHelper.defaultAction("Make moderator".localize())
            { [weak self] action in
                self?.makeModerator(member: member)
            })
        }

        if room.canUnmakeUserModerator(userId: member.userId) {
            // Action to unmake someone a moderator for a room.
            alert.addAction(AlertHelper.defaultAction("Reset to normal user".localize())
            { [weak self] action in
                self?.unmakeModerator(member: member)
            })
        }
        
        if room.canKickUser(userId: member.userId) {
            // Action to kick someone from a room.
            alert.addAction(AlertHelper.destructiveAction("Kick".localize())
            { [weak self] action in
                self?.kickMember(member: member)
            })
        }
        
        if !alert.actions.isEmpty {
            alert.addAction(AlertHelper.cancelAction())

            if let popoverController = alert.popoverPresentationController,
                let cell = tableView.cellForRow(at: indexPath) 
            {
                popoverController.sourceView = cell
                popoverController.sourceRect = cell.bounds
            }

            present(alert, animated: true)
        }
    }
    
    // Need to auto resize the header view.
    // From here: https://useyourloaf.com/blog/variable-height-table-view-header/
    //
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let headerView = tableView.tableHeaderView else {
            return
        }
        
        // The table view header is created with the frame size set in
        // the Storyboard. Calculate the new size and reset the header
        // view to trigger the layout.
        // Calculate the minimum height of the header view that allows
        // the text label to fit its preferred width.
        let size = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        
        if headerView.frame.size.height != size.height {
            headerView.frame.size.height = size.height
            
            // Need to set the header view property of the table view
            // to trigger the new layout. Be careful to only do this
            // once when the height changes or we get stuck in a layout loop.
            tableView.tableHeaderView = headerView
            
            // Now that the table view header is sized correctly have
            // the table view redo its layout so that the cells are
            // correcly positioned for the new header size.
            // This only seems to be necessary on iOS 9.
            tableView.layoutIfNeeded()
        }
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        if let vc = segue.destination as? CircleEditDetailsViewController {
            vc.circle = room
            vc.delegate = self
        }
    }
    
    func changeJoinStatus(open: Bool) {
        guard let room = room else {
            return
        }

        workingOverlay.isHidden = false

        room.setJoinRule(open ? .public : .invite) { [weak self] response in
            self?.workingOverlay.isHidden = true
            self?.updateViews()
        }
    }
    
    func leaveCircle() {
        guard let room = room else {
            return
        }

        workingOverlay.isHidden = false

        UIApplication.shared.leaveRoom(room: room) { [weak self] room, success in
            self?.workingOverlay.isHidden = true
            if success {
                // Close this view controller
                self?.navigationController?.popViewController(animated: true)
            }
            else {
                self?.updateViews()
            }
        }
    }
    
    func deleteCircle() {
        guard let room = room else {
            return
        }

        workingOverlay.isHidden = false

        Task {
            do {
                try await Circles.shared.delete(room: room)

                await MainActor.run() {
                    workingOverlay.isHidden = true

                    navigationController?.popViewController(animated: true)
                }
            }
            catch {
                await MainActor.run() {
                    workingOverlay.isHidden = true
                    updateViews()

                    AlertHelper.present(self, message: error.localizedDescription)
                }
            }
        }
    }


    // MARK: CircleEditDetailsViewControllerDelegate

    public func updateDetails(name: String?, topic: String?, done: ((Bool) -> Void)?) {
        guard let room = room, name != nil || topic != nil else {
            return
        }

        let group = DispatchGroup()
        var success = true

        if let name = name {
            group.enter()

            room.setName(name) { response in
                if response.isFailure {
                    success = false
                }

                group.leave()
            }
        }

        if let topic = topic {
            group.enter()

            room.setTopic(topic) { response in
                if response.isFailure {
                    success = false
                }

                group.leave()
            }
        }

        group.notify(queue: DispatchQueue.global()) {
            DispatchQueue.main.async {
                done?(success)
            }
        }
    }


    // MARK: Room actions

    @MainActor
    public func makeAdmin(member: MXRoomMember) {
        wrapWithOverlay { room, completion in
            room.setPowerLevel(ofUser: member.userId, powerLevel: 100, completion: completion)
        }
    }

    @MainActor
    public func makeModerator(member: MXRoomMember) {
        wrapWithOverlay { room, completion in
            room.setPowerLevel(ofUser: member.userId, powerLevel: 50, completion: completion)
        }
    }

    public func unmakeModerator(member: MXRoomMember) {
        wrapWithOverlay { room, completion in
            room.setPowerLevel(ofUser: member.userId, powerLevel: 0, completion: completion)
        }
    }

    public func kickMember(member: MXRoomMember) {
        wrapWithOverlay { room, completion in
            room.kickUser(member.userId, reason: "", completion: completion)
        }
    }

    private func wrapWithOverlay(_ callback: @escaping (_ room: MXRoom, _ completion: @escaping (_ response: MXResponse<Void>) -> Void) -> Void) {
        workingOverlay.isHidden = false

        Task { @MainActor [weak self] in
            guard let room = self?.room else {
                self?.workingOverlay.isHidden = true

                return
            }

            callback(room) { _ in
                self?.workingOverlay.isHidden = true
            }
        }
    }
}
