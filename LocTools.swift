//
//  LocTools.swift
//  Circulo
//
//  Created by Benjamin Erhart on 13.04.23.
//

import Foundation

struct LocString: Comparable {

    let key: String

    let value: String

    var comment: Set<String>

    init(_ key: String, _ value: String, _ comment: Set<String>? = nil) {
        self.key = key
        self.value = value
        self.comment = comment ?? []
    }

    static func < (lhs: LocString, rhs: LocString) -> Bool {
        return lhs.key < rhs.key
    }

    static func == (lhs: LocString, rhs: LocString) -> Bool {
        return lhs.key == rhs.key
    }
}

class LocTools {

    private static let androidRegex = try? NSRegularExpression(pattern: "<string name=\"(.+?)\">(.+?)</string>")

    // There's some dummy text in XIBs, which we want to ignore.
    private static let xibIgnorelist = [
        "foobar", "lorem ipsum", ">>", "^<x> Untrusted New Devices for <user>$",
        "^@bob:matrix.org$", "^Action 1$", "^Action 2$", "^Button$",
        "^Label$", "^Subtitle$", "^Title$", "^\\\\u\\{e1b1\\}$",
        "^device key fingerprint$"]


    let toolname: String

    var map = [String: String]()
    var translation = [String: [String: String]]()

    var ignorelistCache = [String: NSRegularExpression]()

    init(_ toolname: String) {
        self.toolname = toolname
    }

    func resolve(_ path: String) -> URL {
        let cwd = URL(fileURLWithPath: FileManager.default.currentDirectoryPath)
        let base = URL(fileURLWithPath: ProcessInfo.processInfo.environment["LOC_TOOLS_BASE"]!, relativeTo: cwd)

        return URL(fileURLWithPath: path, relativeTo: base)
    }

    func parse(androidStringsFile file: URL, reverse: Bool = false) throws -> [String: String] {
        let content = try String(contentsOf: file)

        var dictionary = [String: String]()

        for match in Self.androidRegex?.matches(in: content, range: NSRange(content.startIndex ..< content.endIndex, in: content)) ?? [] {
            guard match.numberOfRanges > 2,
                  let k = Range(match.range(at: 1), in: content),
                  let v = Range(match.range(at: 2), in: content)
            else {
                continue
            }

            let key = String(content[k])
            let value = String(content[v])

            if reverse {
                dictionary[value] = key
            }
            else {
                dictionary[key] = value
            }
        }

        return dictionary
    }

    func localize(_ lang: String, _ sourceString: String) -> String? {
        var replacement = "%s"
        var replacedString = sourceString.replacingOccurrences(of: "%", with: replacement)
        var prefixCutoff = false

        if sourceString != replacedString {
            if map[replacedString] == nil {
                replacement = "%d"
                replacedString = sourceString.replacingOccurrences(of: "%", with: replacement)
            }

            if map[replacedString] == nil && sourceString.hasPrefix("% ") {
                replacedString = String(sourceString.suffix(from: sourceString.index(sourceString.startIndex, offsetBy: 2)))
                prefixCutoff = true
            }
        }

        guard let key = map[replacedString] else {
            return nil
        }

        let translatedString = translation[lang]?[key]

        if replacedString != sourceString {
            if prefixCutoff, let translatedString = translatedString {
                return "% \(translatedString)"
            }

            return translatedString?.replacingOccurrences(of: replacement, with: "%")
        }

        return translatedString
    }

    func getAllFiles(with extensions: [String], in directories: [URL]) -> [URL] {
        var files = [URL]()

        for dir in directories {
            let enumerator = FileManager.default.enumerator(at: dir, includingPropertiesForKeys: [.isDirectoryKey]) { url, error in
                print(error.localizedDescription)

                return true
            }

            while let file = enumerator?.nextObject() as? URL {
                if (try? file.resourceValues(forKeys: [.isDirectoryKey]).isDirectory) ?? false {
                    continue
                }

                for `extension` in extensions {
                    if file.pathExtension == `extension` {
                        files.append(file)
                    }
                }
            }
        }

        return files
    }

    func isIgnored(_ value: String) -> Bool {
        // Ignore keys with one character. That's most probably references to
        // an icon font.
        guard value.count > 1 else {
            return true
        }

        let range = NSRange(value.startIndex ..< value.endIndex, in: value)

        for pattern in Self.xibIgnorelist {
            let regex = ignorelistCache[pattern] ?? (try? NSRegularExpression(pattern: pattern, options: .caseInsensitive))
            ignorelistCache[pattern] = regex

            if regex?.numberOfMatches(in: value, range: range) ?? 0 > 0 {
                return true
            }
        }

        return false
    }

    func writeStrings(file: URL, _ content: [LocString]) throws {
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US_POSIX")
        df.dateStyle = .short
        df.timeStyle = .none

        let day = df.string(from: Date())

        df.setLocalizedDateFormatFromTemplate("YYYY")

        var output = "//\n"
        output += "// \(file.lastPathComponent)\n"
        output += "// Circulo\n"
        output += "// \n"
        output += "// Created by \(toolname) on \(day).\n"
        output += "//\n"
        output += "\n"

        for item in content.sorted() {
            if !item.comment.isEmpty {
                output += "/* \(item.comment.sorted().joined(separator: ", ")) */\n"
            }

            output += "\"\(item.key)\" = \"\(item.value)\";\n"
            output += "\n"
        }

        try FileManager.default.createDirectory(at: file.deletingLastPathComponent(), withIntermediateDirectories: true)

        try output.write(to: file, atomically: true, encoding: .utf8)
    }
}
