//
//  loc-tools.swift
//  Circulo
//
//  Created by Benjamin Erhart on 13.04.23.
//

import Foundation

let lt = LocTools("loc-tools.sh")

let roots = [
    lt.resolve("Circulo/"),
    lt.resolve("CirculoNSE/"),
    lt.resolve("Pods/Keanu"),
    lt.resolve("Pods/KeanuCore")]

let regex = try? NSRegularExpression(pattern: "\"((?:\\\\\"|[^\"])+?)\"\\.localize\\(")

let enLocFile = lt.resolve("Circulo/en.lproj/Localizable.strings")

let languages = ["ar": "ar", "de": "de", "es": "es-419", "fa": "fa", "fi": "fi", "fr": "fr", "it": "it", "nb-rNO": "nb", "pt-rBR": "pt-BR", "ru": "ru", "sr": "sr", "uk": "uk", "zh-rCN": "zh-Hans"]

let targetFileMask = "Circulo/%@.lproj/Localizable.strings"
let mapFile = lt.resolve("../circulo-for-android/circuloapp/src/main/res/values/strings.xml")
let locFileMask = "../circulo-for-android/circuloapp/src/main/res/values-%@/strings.xml"


// MARK: Main

var source = [String: LocString]()

for file in lt.getAllFiles(with: ["swift"], in: roots) {
    guard let content = try? String(contentsOf: file) else {
        continue
    }

    for match in regex?.matches(in: content, range: NSRange(content.startIndex ..< content.endIndex, in: content)) ?? [] {
        guard match.numberOfRanges > 1,
              let r = Range(match.range(at: 1), in: content)
        else {
            continue
        }

        let key = String(content[r])

        if source[key] == nil {
            source[key] = LocString(key, key, [file.lastPathComponent])
        }
        else {
            source[key]?.comment.insert(file.lastPathComponent)
        }
    }
}

let tempFile = FileManager.default.temporaryDirectory.appendingPathComponent("temp.strings")

for file in lt.getAllFiles(with: ["xib", "storyboard"], in: roots) {
    let task = Process()
    task.launchPath = "/usr/bin/env"
    task.arguments = ["ibtool", file.path, "--generate-strings-file", tempFile.path]
    task.launch()
    task.waitUntilExit()

    guard let strings = NSDictionary(contentsOf: tempFile) as? Dictionary<String, String> else {
        continue
    }

    // The key is a random identifier of the Interface Builder UIView element,
    // the value is the actual text to localize.
    for key in strings.values {
        if lt.isIgnored(key) {
            continue
        }

        if source[key] == nil {
            source[key] = LocString(key, key, [file.lastPathComponent])
        }
        else {
            source[key]?.comment.insert(file.lastPathComponent)
        }
    }
}

try lt.writeStrings(file: enLocFile, source.map({ $0.value }))


lt.map = try lt.parse(androidStringsFile: mapFile, reverse: true)

for l in languages {
    lt.translation[l.key] = try lt.parse(androidStringsFile: lt.resolve(String(format: locFileMask, l.key)))

    var target = [String: String]()

    for item in source {
        if let translated = lt.localize(l.key, item.value.value) {
            target[item.key] = translated
        }
    }

    try lt.writeStrings(file: lt.resolve(String(format: targetFileMask, l.value)),
                        target.map({ LocString($0.key, $0.value, source[$0.key]?.comment) }))
}
