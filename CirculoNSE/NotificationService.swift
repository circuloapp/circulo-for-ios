//
//  NotificationService.swift
//  CirculoNSE
//
//  Created by N-Pex on 24.11.21.
//

import UserNotifications
import MatrixSDK
import KeanuCore

class NotificationService: UNNotificationServiceExtension {
    
    private static var backgroundSyncService: MXBackgroundSyncService?

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = request.content.mutableCopy() as? UNMutableNotificationContent

        KeanuCore.setUp(with: Config.self)
        KeanuCore.setUpMatrix()
        DomainHelper.shared.prepareSessionForActiveAccounts()
        
        guard let account = Circles.shared.account else {
            contentHandler(request.content) // Use original content
            return
        }
        
        if Self.backgroundSyncService?.credentials != account.mxCredentials {
            Self.backgroundSyncService = MXBackgroundSyncService(withCredentials: account.mxCredentials)
        }

        if let bestAttemptContent = bestAttemptContent,
           let eventId = bestAttemptContent.userInfo["event_id"] as? String,
           let roomId = bestAttemptContent.userInfo["room_id"] as? String
        {
            Self.backgroundSyncService?.event(withEventId: eventId, inRoom: roomId) { response in
                if response.isSuccess, let event = response.value {
                    if !event.showNotification {
                        // Don't deliver the notification to the user.
                        return contentHandler(UNNotificationContent())
                    }

                    bestAttemptContent.body = event.notificationBody
                    bestAttemptContent.sound = event.getNotificationSound(false)

                    UNUserNotificationCenter.current().getNotificationSettings { settings in
                        bestAttemptContent.sound = event.getNotificationSound(settings.criticalAlertSetting == .enabled)

                        contentHandler(bestAttemptContent)
                    }

                    return
                }

                contentHandler(bestAttemptContent)
            }
        } 
        else {
            // Fallback
            contentHandler(request.content)
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent = bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
